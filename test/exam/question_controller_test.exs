# defmodule Quizler.ExamQuestionControllerTest do
#   use Quizler.ConnCase
#
#   alias Quizler.ExamQuestion
#   @valid_attrs %{answer: "some content", choices: "some content", exam_id: 42, question: "some content", type: 42}
#   @invalid_attrs %{}
#
#   test "lists all entries on index", %{conn: conn} do
#     conn = get conn, exam_question_path(conn, :index)
#     assert html_response(conn, 200) =~ "Listing exam questions"
#   end
#
#   test "renders form for new resources", %{conn: conn} do
#     conn = get conn, exam_question_path(conn, :new)
#     assert html_response(conn, 200) =~ "New exam question"
#   end
#
#   test "creates resource and redirects when data is valid", %{conn: conn} do
#     conn = post conn, exam_question_path(conn, :create), exam_question: @valid_attrs
#     assert redirected_to(conn) == exam_question_path(conn, :index)
#     assert Repo.get_by(ExamQuestion, @valid_attrs)
#   end
#
#   test "does not create resource and renders errors when data is invalid", %{conn: conn} do
#     conn = post conn, exam_question_path(conn, :create), exam_question: @invalid_attrs
#     assert html_response(conn, 200) =~ "New exam question"
#   end
#
#   test "shows chosen resource", %{conn: conn} do
#     exam_question = Repo.insert! %ExamQuestion{}
#     conn = get conn, exam_question_path(conn, :show, exam_question)
#     assert html_response(conn, 200) =~ "Show exam question"
#   end
#
#   test "renders page not found when id is nonexistent", %{conn: conn} do
#     assert_error_sent 404, fn ->
#       get conn, exam_question_path(conn, :show, -1)
#     end
#   end
#
#   test "renders form for editing chosen resource", %{conn: conn} do
#     exam_question = Repo.insert! %ExamQuestion{}
#     conn = get conn, exam_question_path(conn, :edit, exam_question)
#     assert html_response(conn, 200) =~ "Edit exam question"
#   end
#
#   test "updates chosen resource and redirects when data is valid", %{conn: conn} do
#     exam_question = Repo.insert! %ExamQuestion{}
#     conn = put conn, exam_question_path(conn, :update, exam_question), exam_question: @valid_attrs
#     assert redirected_to(conn) == exam_question_path(conn, :show, exam_question)
#     assert Repo.get_by(ExamQuestion, @valid_attrs)
#   end
#
#   test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
#     exam_question = Repo.insert! %ExamQuestion{}
#     conn = put conn, exam_question_path(conn, :update, exam_question), exam_question: @invalid_attrs
#     assert html_response(conn, 200) =~ "Edit exam question"
#   end
#
#   test "deletes chosen resource", %{conn: conn} do
#     exam_question = Repo.insert! %ExamQuestion{}
#     conn = delete conn, exam_question_path(conn, :delete, exam_question)
#     assert redirected_to(conn) == exam_question_path(conn, :index)
#     refute Repo.get(ExamQuestion, exam_question.id)
#   end
# end
