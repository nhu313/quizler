# defmodule Quizler.ExamControllerTest do
#   use Quizler.ConnCase
#
#   alias Quizler.Exam
#   @valid_attrs %{name: "some content"}
#   @invalid_attrs %{}
#
#   test "lists all entries on index", %{conn: conn} do
#     conn = get conn, exam_path(conn, :index)
#     assert html_response(conn, 200) =~ "Listing exams"
#   end
#
#   test "renders form for new resources", %{conn: conn} do
#     conn = get conn, exam_path(conn, :new)
#     assert html_response(conn, 200) =~ "New exam"
#   end
#
#   test "creates resource and redirects when data is valid", %{conn: conn} do
#     conn = post conn, exam_path(conn, :create), exam: @valid_attrs
#     assert redirected_to(conn) == exam_path(conn, :index)
#     assert Repo.get_by(Exam, @valid_attrs)
#   end
#
#   test "does not create resource and renders errors when data is invalid", %{conn: conn} do
#     conn = post conn, exam_path(conn, :create), exam: @invalid_attrs
#     assert html_response(conn, 200) =~ "New exam"
#   end
#
#   test "shows chosen resource", %{conn: conn} do
#     exam = Repo.insert! %Exam{}
#     conn = get conn, exam_path(conn, :show, exam)
#     assert html_response(conn, 200) =~ "Show exam"
#   end
#
#   test "renders page not found when id is nonexistent", %{conn: conn} do
#     assert_error_sent 404, fn ->
#       get conn, exam_path(conn, :show, -1)
#     end
#   end
#
#   test "renders form for editing chosen resource", %{conn: conn} do
#     exam = Repo.insert! %Exam{}
#     conn = get conn, exam_path(conn, :edit, exam)
#     assert html_response(conn, 200) =~ "Edit exam"
#   end
#
#   test "updates chosen resource and redirects when data is valid", %{conn: conn} do
#     exam = Repo.insert! %Exam{}
#     conn = put conn, exam_path(conn, :update, exam), exam: @valid_attrs
#     assert redirected_to(conn) == exam_path(conn, :show, exam)
#     assert Repo.get_by(Exam, @valid_attrs)
#   end
#
#   test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
#     exam = Repo.insert! %Exam{}
#     conn = put conn, exam_path(conn, :update, exam), exam: @invalid_attrs
#     assert html_response(conn, 200) =~ "Edit exam"
#   end
#
#   test "deletes chosen resource", %{conn: conn} do
#     exam = Repo.insert! %Exam{}
#     conn = delete conn, exam_path(conn, :delete, exam)
#     assert redirected_to(conn) == exam_path(conn, :index)
#     refute Repo.get(Exam, exam.id)
#   end
# end
