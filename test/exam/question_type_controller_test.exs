# defmodule Quizler.ExamQuestionTypeControllerTest do
#   use Quizler.ConnCase
#
#   alias Quizler.ExamQuestionType
#   @valid_attrs %{instructions: "some content", name: "some content"}
#   @invalid_attrs %{}
#
#   test "lists all entries on index", %{conn: conn} do
#     conn = get conn, exam_question_type_path(conn, :index)
#     assert html_response(conn, 200) =~ "Listing exam question types"
#   end
#
#   test "renders form for new resources", %{conn: conn} do
#     conn = get conn, exam_question_type_path(conn, :new)
#     assert html_response(conn, 200) =~ "New exam question type"
#   end
#
#   test "creates resource and redirects when data is valid", %{conn: conn} do
#     conn = post conn, exam_question_type_path(conn, :create), exam_question_type: @valid_attrs
#     assert redirected_to(conn) == exam_question_type_path(conn, :index)
#     assert Repo.get_by(ExamQuestionType, @valid_attrs)
#   end
#
#   test "does not create resource and renders errors when data is invalid", %{conn: conn} do
#     conn = post conn, exam_question_type_path(conn, :create), exam_question_type: @invalid_attrs
#     assert html_response(conn, 200) =~ "New exam question type"
#   end
#
#   test "shows chosen resource", %{conn: conn} do
#     exam_question_type = Repo.insert! %ExamQuestionType{}
#     conn = get conn, exam_question_type_path(conn, :show, exam_question_type)
#     assert html_response(conn, 200) =~ "Show exam question type"
#   end
#
#   test "renders page not found when id is nonexistent", %{conn: conn} do
#     assert_error_sent 404, fn ->
#       get conn, exam_question_type_path(conn, :show, -1)
#     end
#   end
#
#   test "renders form for editing chosen resource", %{conn: conn} do
#     exam_question_type = Repo.insert! %ExamQuestionType{}
#     conn = get conn, exam_question_type_path(conn, :edit, exam_question_type)
#     assert html_response(conn, 200) =~ "Edit exam question type"
#   end
#
#   test "updates chosen resource and redirects when data is valid", %{conn: conn} do
#     exam_question_type = Repo.insert! %ExamQuestionType{}
#     conn = put conn, exam_question_type_path(conn, :update, exam_question_type), exam_question_type: @valid_attrs
#     assert redirected_to(conn) == exam_question_type_path(conn, :show, exam_question_type)
#     assert Repo.get_by(ExamQuestionType, @valid_attrs)
#   end
#
#   test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
#     exam_question_type = Repo.insert! %ExamQuestionType{}
#     conn = put conn, exam_question_type_path(conn, :update, exam_question_type), exam_question_type: @invalid_attrs
#     assert html_response(conn, 200) =~ "Edit exam question type"
#   end
#
#   test "deletes chosen resource", %{conn: conn} do
#     exam_question_type = Repo.insert! %ExamQuestionType{}
#     conn = delete conn, exam_question_type_path(conn, :delete, exam_question_type)
#     assert redirected_to(conn) == exam_question_type_path(conn, :index)
#     refute Repo.get(ExamQuestionType, exam_question_type.id)
#   end
# end
