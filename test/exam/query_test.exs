defmodule Quizler.ExamQueryTest do
  use Quizler.ModelCase

  alias Quizler.{ExamQuery, ExamFactory}

  test "with_questions_and_instructions" do
    type1 = ExamFactory.create_question_type("Ethics")
    type2 = ExamFactory.create_question_type("Synonyms", "Match words with same meaning")

    exam = ExamFactory.create_exam
    question2 = ExamFactory.create_question(exam, type2.id)
    question1 = ExamFactory.create_question(exam, type1.id)
    question3 = ExamFactory.create_question(exam, type1.id)

    exam = ExamQuery.with_questions_and_instructions(exam.id)
    [group1, group2] = Enum.sort(exam.grouped_questions, &(&1.name < &2.name))

    assert group1.name == type1.name
    group1_question_ids = group1.questions |> Enum.map(&(&1.id)) |> Enum.sort
    assert group1_question_ids == [question1.id, question3.id]

    assert group2.name == type2.name
    assert hd(group2.questions).id == question2.id
  end

  test "with_questions_and_instructions add answer to choices" do
    exam = ExamFactory.create_exam
    question = ExamFactory.create_question(exam)

    exam = ExamQuery.with_questions_and_instructions(exam.id)
    [group] = exam.grouped_questions

    [saved_question] = group.questions
    assert Enum.member?(saved_question.choices, question.answer)
  end

  test "with_question_and_answers add answer" do
    exam = ExamFactory.create_exam
    question1 = ExamFactory.create_question(exam)
    question2 = ExamFactory.create_question(exam)

    answers = %{"#{question1.id}" => question1.answer, "#{question2.id}" => hd(question2.choices)}
    exam = ExamQuery.with_questions_and_answers(exam.id, answers)
    [group] = exam.grouped_questions

    [saved_question1, saved_question2] = group.questions |> Enum.sort(&(&1.id < &2.id))

    assert saved_question1.id == question1.id
    assert saved_question1.user_answer == question1.answer
    assert saved_question2.user_answer == hd(question2.choices)
  end
end
