defmodule Quizler.ExamQuestionTest do
  use Quizler.ModelCase

  alias Quizler.ExamQuestion

  @valid_attrs %{answer: "some content", choices: ["some", "content"], exam_id: 42, question: "some content", type_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ExamQuestion.changeset(%ExamQuestion{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ExamQuestion.changeset(%ExamQuestion{}, @invalid_attrs)
    refute changeset.valid?
  end
end
