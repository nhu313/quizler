defmodule Quizler.ExamFactory do
  alias Quizler.{Repo, Exam, ExamQuestion, ExamQuery, ExamQuestionType}

  def create_question_type(name, instructions \\ "") do
    %{name: name, instructions: instructions}
    |> ExamQuestionType.new_changeset
    |> Repo.insert!
  end

  def create_exam do
    %{name: "PA sample test"}
    |> Exam.new_changeset
    |> Repo.insert!
  end

  def create_question(exam, type_id \\ 1) do
    %{question: "Question #{type_id}", exam_id: exam.id,
      answer: "C", choices: ["A", "B", "D"], type_id: type_id}
    |> ExamQuestion.new_changeset
    |> Repo.insert!
  end
end
