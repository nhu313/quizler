defmodule Quizler.DbCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias Quizler.Repo
      import Quizler.DbCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Quizler.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Quizler.Repo, {:shared, self()})
    end

    :ok
  end

end
