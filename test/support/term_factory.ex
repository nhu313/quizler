defmodule Quizler.TermFactory do
  alias Quizler.{Repo, Deck, Term, SpanishTerm, Group}

  def create_translation(term) do
    %{term: "embargo preventivo", source_term_id: term.id}
    |> SpanishTerm.new_changeset
    |> Repo.insert!
  end

  def create_term(deck) do
    %{term: "lien", deck_id: deck.id}
    |> Term.new_changeset
    |> Repo.insert!
  end

  def create_deck(params \\ %{}) do
    %{group_id: 17, name: "Day 1"}
    |> Map.merge(params)
    |> Deck.new_changeset()
    |> Repo.insert!
  end

  def create_group do
    %{name: "Group 1"}
    |> Group.new_changeset
    |> Repo.insert!
  end

  def create_deck_with_terms(group, num_of_terms) do
    deck = create_deck(%{group_id: group.id})
    terms = 1..num_of_terms
            |> Enum.map(fn _ -> create_term(deck) end)
    Map.put(deck, :terms, terms)
  end
end
