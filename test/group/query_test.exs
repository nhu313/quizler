defmodule Quizler.GroupQueryTest do
    use Quizler.ModelCase
    alias Quizler.{Group, Deck, QuizResult, GroupQuery}

    @user_id 15425

    test "next group" do
      create_group(%{name: "legal terms 4", step: 4})
      group2 = create_group(%{name: "legal terms 2", step: 2})
      group1 = create_group(%{name: "legal terms 1", step: 1})
      create_group(%{name: "legal terms 3", step: 3})

      next_group = GroupQuery.next(group1)
      assert next_group.id == group2.id
    end

    test "next group when current group has no step" do
      group1 = create_group(%{name: "legal terms 1"})
      group2 = create_group(%{name: "legal terms 2", step: 1})

      next_group = GroupQuery.next(group1)

      assert next_group.id == group2.id
    end

    test "separate deck by status" do
      group = create_group(%{name: "legal terms"})
      [deck1, deck2, deck3, deck4] = create_decks(group, 4)
      mark_as_done([deck1, deck2], @user_id)

      group = GroupQuery.with_deck_split_by_status(group.id, @user_id)

      assert group.current_deck == deck3
      assert group.done_decks == [deck1, deck2]
      assert group.not_done_decks == [deck4]
    end

    test "only pull back your status" do
      group = create_group(%{name: "legal terms"})
      [deck1, deck2] = create_decks(group, 2)

      another_user_id = 141535
      mark_as_done([deck1, deck2], another_user_id)

      group = GroupQuery.with_deck_split_by_status(group.id, @user_id)

      assert group.current_deck == deck1
      assert group.done_decks == []
      assert group.not_done_decks == [deck2]
    end

    defp mark_as_done(decks, user_id) do
      decks |> Enum.map(&(create_done_deck(&1, user_id)))
    end

    defp create_done_deck(deck, user_id) do
      %{deck_id: deck.id, user_id: user_id}
      |> QuizResult.new_changeset
      |> Repo.insert!
    end

    defp create_group(params) do
      params |> Group.new_changeset |> Repo.insert!
    end

    defp create_decks(group, size) do
      Enum.map(1..size, &(create_deck(group, &1)))
    end

    defp create_deck(group, step) do
      %{name: "deck #{step}", group_id: group.id, step: step, user_id: @user_id}
      |> Deck.new_changeset
      |> Repo.insert!
    end
end
