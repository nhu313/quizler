# defmodule Quizler.SpanishTermControllerTest do
#   use Quizler.ConnCase
#
#   alias Quizler.SpanishTerm
#   @valid_attrs %{definition: "some content", source_term_id: 42, term: "some content"}
#   @invalid_attrs %{}
#
#   test "lists all entries on index", %{conn: conn} do
#     conn = get conn, spanish_term_path(conn, :index)
#     assert html_response(conn, 200) =~ "Listing spanish terms"
#   end
#
#   test "renders form for new resources", %{conn: conn} do
#     conn = get conn, spanish_term_path(conn, :new)
#     assert html_response(conn, 200) =~ "New spanish term"
#   end
#
#   test "creates resource and redirects when data is valid", %{conn: conn} do
#     conn = post conn, spanish_term_path(conn, :create), spanish_term: @valid_attrs
#     assert redirected_to(conn) == spanish_term_path(conn, :index)
#     assert Repo.get_by(SpanishTerm, @valid_attrs)
#   end
#
#   test "does not create resource and renders errors when data is invalid", %{conn: conn} do
#     conn = post conn, spanish_term_path(conn, :create), spanish_term: @invalid_attrs
#     assert html_response(conn, 200) =~ "New spanish term"
#   end
#
#   test "shows chosen resource", %{conn: conn} do
#     spanish_term = Repo.insert! %SpanishTerm{}
#     conn = get conn, spanish_term_path(conn, :show, spanish_term)
#     assert html_response(conn, 200) =~ "Show spanish term"
#   end
#
#   test "renders page not found when id is nonexistent", %{conn: conn} do
#     assert_error_sent 404, fn ->
#       get conn, spanish_term_path(conn, :show, -1)
#     end
#   end
#
#   test "renders form for editing chosen resource", %{conn: conn} do
#     spanish_term = Repo.insert! %SpanishTerm{}
#     conn = get conn, spanish_term_path(conn, :edit, spanish_term)
#     assert html_response(conn, 200) =~ "Edit spanish term"
#   end
#
#   test "updates chosen resource and redirects when data is valid", %{conn: conn} do
#     spanish_term = Repo.insert! %SpanishTerm{}
#     conn = put conn, spanish_term_path(conn, :update, spanish_term), spanish_term: @valid_attrs
#     assert redirected_to(conn) == spanish_term_path(conn, :show, spanish_term)
#     assert Repo.get_by(SpanishTerm, @valid_attrs)
#   end
#
#   test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
#     spanish_term = Repo.insert! %SpanishTerm{}
#     conn = put conn, spanish_term_path(conn, :update, spanish_term), spanish_term: @invalid_attrs
#     assert html_response(conn, 200) =~ "Edit spanish term"
#   end
#
#   test "deletes chosen resource", %{conn: conn} do
#     spanish_term = Repo.insert! %SpanishTerm{}
#     conn = delete conn, spanish_term_path(conn, :delete, spanish_term)
#     assert redirected_to(conn) == spanish_term_path(conn, :index)
#     refute Repo.get(SpanishTerm, spanish_term.id)
#   end
# end
