defmodule Quizler.UserLanguageTest do
  use Quizler.ModelCase

  alias Quizler.UserLanguage

  @valid_attrs %{language_id: 42, user_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserLanguage.changeset(%UserLanguage{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserLanguage.changeset(%UserLanguage{}, @invalid_attrs)
    refute changeset.valid?
  end
end
