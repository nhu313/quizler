defmodule Quizler.LanguageSaverTest do
  use Quizler.ModelCase
  alias Quizler.{Language, LanguageSaver, LanguageQuery, UserQuery, User}

  setup do
    language_names = ["Thai", "Lao", "Arabic"]
    languages = language_names
            |> Enum.map(&(Repo.insert!(%Language{name: &1})))
            |> Enum.sort

    language_ids = Enum.map(languages, &(Integer.to_string(&1.id)))

    user = Repo.insert! %User{name: "cookie",
                              email: "cookie@monster.com"}
    {:ok, user: user, languages: languages, language_ids: language_ids}
  end

  test "add new language to user's profile", params do
    LanguageSaver.save_user_languages(params.user, params.language_ids)
    assert params.languages == saved_languages(params.user)
  end

  defp saved_languages(user) do
    saved_user = UserQuery.all(user.id)
    Enum.sort(saved_user.languages)
  end

  test "when user didn't select a language", %{user: user} do
    LanguageSaver.save_user_languages(user, nil)

    assert [] == saved_languages(user)
  end

  test "delete all languages from user's profile", %{language_ids: language_ids, user: user} do
    LanguageSaver.save_user_languages(user, language_ids)
    LanguageSaver.save_user_languages(user, [])
    assert [] == saved_languages(user)
  end

  test "add and delete languages to user's profile", %{user: user} = params  do
    [language_id1, language_id2, language_id3] = params.language_ids
    [language1, language2, _] = params.languages
    LanguageSaver.save_user_languages(user, [language_id2, language_id3])

    LanguageSaver.save_user_languages(user, [language_id2, language_id1])
    assert [language1, language2] == saved_languages(user)
  end

  test "add languages that doesn't exist in the database", %{user: user} do
    language_name = "Italian"
    LanguageSaver.save_user_languages(user, [language_name])
    [language] = saved_languages(user)
    assert language.name == language_name
  end

  test "when language params is the language name instead of the language id", %{user: user, languages: languages} do
    [thai | _] = languages
    LanguageSaver.save_user_languages(user, [thai.name])
    [saved_language] = saved_languages(user)

    assert saved_language.id == thai.id
  end
end
