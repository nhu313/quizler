defmodule Quizler.Deck.GameFormatterTest do
  use ExUnit.Case
  alias Quizler.{Deck.GameFormatter, Term}

  test "format terms for match game" do
    size = 2
    terms = [term1, term2] = create_terms(size)

    cards = GameFormatter.cards_for_matching(terms)
    assert length(cards) == size * 2
    assert Enum.member?(cards, %{term_id: term1.id, name: term1.term, status: ""})
    assert Enum.member?(cards, %{term_id: term1.id, name: term1.definition, status: ""})
    assert Enum.member?(cards, %{term_id: term2.id, name: term2.term, status: ""})
    assert Enum.member?(cards, %{term_id: term2.id, name: term2.definition, status: ""})
  end

  def create_terms(size) do
    Enum.map(1..size, &create_term/1)
  end

  defp create_term(id) do
    %Term{id: id, term: "term #{id}", definition: "definition #{id}"}
  end

end
