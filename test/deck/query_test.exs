defmodule Quizler.DeckQueryTest do
  use Quizler.ModelCase
  alias Quizler.{Deck, DeckQuery}

  @deck_params %{group_id: 17,
                   name: "Day 1"}

  test "get next step" do
    create_deck(%{step: 5})
    step3 = create_deck(%{step: 3})
    step1 = create_deck(%{step: 1})
    create_deck(%{step: 4})

    next_step = DeckQuery.next(step1)
    assert next_step.id == step3.id
  end

  test "get next step when current step is the last step" do
    create_deck(%{step: 3})
    last_step = create_deck(%{step: 5})
    create_deck(%{step: 1})
    create_deck(%{step: 4})

    refute DeckQuery.next(last_step)
  end

  defp create_deck(params) do
    @deck_params
    |> Map.merge(params)
    |> Deck.new_changeset()
    |> Repo.insert!
  end
end
