defmodule Quizler.Deck.QuizGeneratorTest do
  use Quizler.DbCase
  alias Quizler.{Deck.QuizGenerator, Term}

  @terms [%Term{id: 1, term: "adjudicate",definition: "to hear or try and determine judicially"},
          %Term{id: 2, term: "affiant",definition: "one who swears to an affidavit; deponent"},
          %Term{id: 3, term: "allege",definition: "To assert a fact in a pleading."},
          %Term{id: 4, term: "amend",definition: "To change."},
          %Term{id: 5, term: "annul",definition: "to make void, as to dissolve the bonds of marriage"},
          %Term{id: 6, term: "appellant",definition: "the party who takes an appeal to a higher court"},
          %Term{id: 7, term: "appellee",definition: "the party against whom an appeal is taken"},
          ]


  test "create questions" do
    [item | _] = QuizGenerator.create(@terms)
    assert item.question
    assert item.answer
    assert item.term_id

    choices = item.choices
    assert length(Enum.uniq(choices)) == 4
    assert Enum.member?(choices, item.answer)
  end
end
