defmodule Quizler.DeckTest do
  use Quizler.ModelCase

  alias Quizler.Deck

  @valid_attrs %{group_id: 42, image: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Deck.changeset(%Deck{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Deck.changeset(%Deck{}, @invalid_attrs)
    refute changeset.valid?
  end
end
