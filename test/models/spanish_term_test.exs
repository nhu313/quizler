defmodule Quizler.SpanishTermTest do
  use Quizler.ModelCase

  alias Quizler.SpanishTerm

  @valid_attrs %{definition: "some content", source_term_id: 42, term: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = SpanishTerm.changeset(%SpanishTerm{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = SpanishTerm.changeset(%SpanishTerm{}, @invalid_attrs)
    refute changeset.valid?
  end
end
