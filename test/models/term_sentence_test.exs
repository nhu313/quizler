defmodule Quizler.TermSentenceTest do
  use Quizler.ModelCase

  alias Quizler.TermSentence

  @valid_attrs %{sentence: "some content", term_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = TermSentence.changeset(%TermSentence{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = TermSentence.changeset(%TermSentence{}, @invalid_attrs)
    refute changeset.valid?
  end
end
