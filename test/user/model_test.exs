defmodule Quizler.UserTest do
  use Quizler.ModelCase

  alias Quizler.User

  @valid_attrs %{email: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.new_changeset(@valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.new_changeset(@invalid_attrs)
    refute changeset.valid?
  end
end
