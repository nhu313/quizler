defmodule Quizler.User.SaverTest do
  use Quizler.ModelCase
  alias Quizler.User.Saver

  @info %Ueberauth.Auth.Info{
    email: "will@freshprince.com",
    first_name: "Will",
    last_name: "Smith",
    name: "Will Smith",
    image: "https://food.com/photo.jpg"
   }

  @auth %Ueberauth.Auth{
   info: @info,
  }

  test "when the user doesn't exist" do
    {:ok, user} = Saver.create_or_find(@auth)

    assert user.id
    assert user.email == @info.email
    assert user.name == @info.first_name
  end

  test "when the user exists" do
    {:ok, user} = Saver.create_or_find(@auth)
    {:ok, second_save_user} = Saver.create_or_find(@auth)
    assert user.id == second_save_user.id
  end

  test "save user photo for url without size" do
    {:ok, user} = Saver.create_or_find(@auth)
    assert user.photo_url == "https://food.com/photo.jpg"
  end

  test "save user photo with size" do
    url = "http://hello.wo?sz=50"
    info = Map.put(@info, :image, url)
    auth = %Ueberauth.Auth{ info: info}

    {:ok, user} = Saver.create_or_find(auth)
    assert user.photo_url == "http://hello.wo"
  end

  @no_name_info %Ueberauth.Auth.Info{
    email: "will@freshprince.com",
    first_name: "",
    image: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50",
    last_name: "", location: nil, name: "", nickname: nil, phone: nil,
    urls: %{profile: nil, website: nil}
  }

  test "when user have no name" do
    auth = %Ueberauth.Auth{ info: @no_name_info}

    {:ok, user} = Saver.create_or_find(auth)

    assert user.id
    assert user.email == @no_name_info.email
    assert user.name == "will"
  end

  test "when user have no first name but has name" do
    name = "pooh"
    info = Map.put(@no_name_info, :name, name)
    auth = %Ueberauth.Auth{ info: info}

    {:ok, user} = Saver.create_or_find(auth)

    assert user.id
    assert user.email == info.email
    assert user.name == info.name
  end
end
