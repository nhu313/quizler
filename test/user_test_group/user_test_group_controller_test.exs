# defmodule Quizler.UserTestGroupControllerTest do
#   use Quizler.ConnCase
#
#   alias Quizler.UserTestGroup
#   @valid_attrs %{group_id: 42, user_id: 42}
#   @invalid_attrs %{}
#
#   test "lists all entries on index", %{conn: conn} do
#     conn = get conn, user_test_group_path(conn, :index)
#     assert html_response(conn, 200) =~ "Listing users test groups"
#   end
#
#   test "renders form for new resources", %{conn: conn} do
#     conn = get conn, user_test_group_path(conn, :new)
#     assert html_response(conn, 200) =~ "New user test group"
#   end
#
#   test "creates resource and redirects when data is valid", %{conn: conn} do
#     conn = post conn, user_test_group_path(conn, :create), user_test_group: @valid_attrs
#     assert redirected_to(conn) == user_test_group_path(conn, :index)
#     assert Repo.get_by(UserTestGroup, @valid_attrs)
#   end
#
#   test "does not create resource and renders errors when data is invalid", %{conn: conn} do
#     conn = post conn, user_test_group_path(conn, :create), user_test_group: @invalid_attrs
#     assert html_response(conn, 200) =~ "New user test group"
#   end
#
#   test "shows chosen resource", %{conn: conn} do
#     user_test_group = Repo.insert! %UserTestGroup{}
#     conn = get conn, user_test_group_path(conn, :show, user_test_group)
#     assert html_response(conn, 200) =~ "Show user test group"
#   end
#
#   test "renders page not found when id is nonexistent", %{conn: conn} do
#     assert_error_sent 404, fn ->
#       get conn, user_test_group_path(conn, :show, -1)
#     end
#   end
#
#   test "renders form for editing chosen resource", %{conn: conn} do
#     user_test_group = Repo.insert! %UserTestGroup{}
#     conn = get conn, user_test_group_path(conn, :edit, user_test_group)
#     assert html_response(conn, 200) =~ "Edit user test group"
#   end
#
#   test "updates chosen resource and redirects when data is valid", %{conn: conn} do
#     user_test_group = Repo.insert! %UserTestGroup{}
#     conn = put conn, user_test_group_path(conn, :update, user_test_group), user_test_group: @valid_attrs
#     assert redirected_to(conn) == user_test_group_path(conn, :show, user_test_group)
#     assert Repo.get_by(UserTestGroup, @valid_attrs)
#   end
#
#   test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
#     user_test_group = Repo.insert! %UserTestGroup{}
#     conn = put conn, user_test_group_path(conn, :update, user_test_group), user_test_group: @invalid_attrs
#     assert html_response(conn, 200) =~ "Edit user test group"
#   end
#
#   test "deletes chosen resource", %{conn: conn} do
#     user_test_group = Repo.insert! %UserTestGroup{}
#     conn = delete conn, user_test_group_path(conn, :delete, user_test_group)
#     assert redirected_to(conn) == user_test_group_path(conn, :index)
#     refute Repo.get(UserTestGroup, user_test_group.id)
#   end
# end
