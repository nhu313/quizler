defmodule Quizler.UserTestGroupTest do
  use Quizler.ModelCase

  alias Quizler.UserTestGroup

  @valid_attrs %{group_id: 42, user_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserTestGroup.changeset(%UserTestGroup{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserTestGroup.changeset(%UserTestGroup{}, @invalid_attrs)
    refute changeset.valid?
  end
end
