defmodule Quizler.ResultSaverTest do
  use Quizler.ModelCase
  alias Quizler.{ResultSaver, ExamFactory}

  @user_id 14135

  test "create_test when there are wrong answers" do
     params =  %{"group_id" => "3", "result" => %{"wrong_answers" => " 13 11 12",
                                                  "term_ids" => "1 6 13 11 12"}}

     {:ok, result} = ResultSaver.create_test(params, @user_id)

     assert result.id
     assert result.group_id == 3
     assert result.wrong_answers == [13, 11, 12]
     assert result.user_id == @user_id
     assert result.term_ids == [1, 6, 13, 11, 12]
  end

  test "create_quiz when there are wrong answers" do
     params =  %{"deck_id" => "3", "result" => %{"wrong_answers" => " 13 11 12"}}
     {:ok, result} = ResultSaver.create_quiz(params, @user_id)

     assert result.id
     assert result.deck_id == 3
     assert result.wrong_answers == [13, 11, 12]
     assert result.user_id == @user_id
  end

  test "create_quiz when there is no wrong answer" do
     params =  %{"deck_id" => "3", "result" => %{"wrong_answers" => ""}}

     {:ok, result} = ResultSaver.create_quiz(params, @user_id)

     assert result.id
     assert result.wrong_answers == []
  end

  test "create_quiz when wrong answer is nil" do
     params =  %{"deck_id" => "3", "result" => %{"wrong_answers" => nil}}

     {:ok, result} = ResultSaver.create_quiz(params, @user_id)

     assert result.id
     assert result.wrong_answers == []
  end

  test "create exam result" do
    exam = ExamFactory.create_exam
    question1 = ExamFactory.create_question(exam)
    question2 = ExamFactory.create_question(exam)
    question3 = ExamFactory.create_question(exam)

    params = %{"exam_id" => exam.id,
               "answers" => %{"#{question1.id}" => "",
                                "#{question2.id}" => question2.answer,
                                "#{question3.id}" => hd(question3.choices)}}

    {:ok, result} = ResultSaver.create_for_exam(params, @user_id)

    wrong_answers = result.wrong_answers |> Enum.sort

    assert result.user_id == @user_id
    assert result.exam_id == exam.id
    assert wrong_answers == [question1.id, question3.id]
  end


end
