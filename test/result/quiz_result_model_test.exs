defmodule Quizler.QuizResultTest do
  use Quizler.ModelCase

  alias Quizler.QuizResult

  @valid_attrs %{deck_id: 42, user_id: 42, wrong_answers: [42]}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = QuizResult.changeset(%QuizResult{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = QuizResult.changeset(%QuizResult{}, @invalid_attrs)
    refute changeset.valid?
  end
end
