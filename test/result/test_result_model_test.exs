defmodule Quizler.TestResultTest do
  use Quizler.ModelCase

  alias Quizler.TestResult

  @valid_attrs %{group_id: 42, user_id: 42, wrong_answers: [42]}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = TestResult.changeset(%TestResult{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = TestResult.changeset(%TestResult{}, @invalid_attrs)
    refute changeset.valid?
  end
end
