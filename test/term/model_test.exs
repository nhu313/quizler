defmodule Quizler.TermTest do
  use Quizler.ModelCase

  alias Ecto.Changeset
  alias Quizler.Term

  @valid_attrs %{definition: "some content", image: "some content", term: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Term.changeset(%Term{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Term.changeset(%Term{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "trim terms and definition" do
    changeset = Term.changeset(%Term{}, %{definition: "    def   ", term: "   jam "})
    assert Changeset.get_field(changeset, :definition) == "def"
    assert Changeset.get_field(changeset, :term) == "jam"
  end
end
