defmodule Quizler.TermQueryTest do
  use Quizler.ModelCase
  alias Quizler.{TermQuery, SpanishTerm, QuizResult, TestResult}
  import Quizler.TermFactory

  @user_id 134329

  test "for_group returns terms user has not seen first" do
    group = create_group
    deck1 = create_deck_with_terms(group, 1)
    deck2 = create_deck_with_terms(group, 1)
    deck3 = create_deck_with_terms(group, 1)
    deck4 = create_deck_with_terms(group, 2)

    save_quiz_result(deck1.id, [hd(deck1.terms).id])
    save_quiz_result(deck3.id, [])
    [term4, term5] = deck4.terms
    save_test_result(group.id, [term4.id], [term4.id, term5.id])

    assert TermQuery.for_group(group.id, @user_id, 1) == deck2.terms
  end

  test "for_group returns terms user got wrong before the right one on QUIZ" do
    group = create_group
    deck1 = create_deck_with_terms(group, 2)

    [_, term2] = deck1.terms
    save_quiz_result(deck1.id, [term2.id])

    assert TermQuery.for_group(group.id, @user_id, 1) == [term2]
  end

  test "for_group returns terms user got wrong before the right one on TEST" do
    group = create_group
    deck1 = create_deck_with_terms(group, 3)

    [term1, term2, term3] = deck1.terms
    save_test_result(group.id, [term2.id], [term1.id, term2.id, term3.id])

    assert TermQuery.for_group(group.id, @user_id, 1) == [term2]
  end

  test "for_group returns wrong answers first for terms seen both on quiz and test" do
    group = create_group
    deck1 = create_deck_with_terms(group, 3)

    [term1, term2, term3] = deck1.terms
    save_quiz_result(deck1.id, [term2.id])
    save_test_result(group.id, [term3.id, term2.id], [term1.id, term2.id, term3.id])

    assert TermQuery.for_group(group.id, @user_id, 2) == [term2, term3]
  end

  test "for_group returns terms get wrong more often first" do
    group = create_group
    deck1 = create_deck_with_terms(group, 3)

    [term1, term2, term3] = deck1.terms

    # number of times user got term2 right = 1/3 = 33%
    save_test_result(group.id, [term2.id], [term2.id])
    save_test_result(group.id, [term2.id], [term2.id])
    save_test_result(group.id, [], [term2.id])

    # number of times user got term3 right = 3/6 = 50%
    save_test_result(group.id, [term3.id], [term3.id])
    save_test_result(group.id, [term3.id], [term3.id])
    save_test_result(group.id, [term3.id], [term3.id])
    save_test_result(group.id, [], [term3.id])
    save_test_result(group.id, [], [term3.id])
    save_test_result(group.id, [], [term3.id])

    # number of times user got term1 right = 2/2 = 100%
    save_test_result(group.id, [], [term1.id])
    save_test_result(group.id, [], [term1.id])

    assert TermQuery.for_group(group.id, @user_id, 2) == [term2, term3]
  end

  test "for_group returns terms seen once before wrong answers" do
    group = create_group
    deck1 = create_deck_with_terms(group, 3)

    [term1, term2, term3] = deck1.terms

    save_test_result(group.id, [term2.id], [term3.id, term2.id, term1.id])
    save_test_result(group.id, [term2.id], [term2.id, term1.id])

    assert TermQuery.for_group(group.id, @user_id, 2) == [term3, term2]
  end

  defp save_test_result(group_id, wrong_answers, term_ids \\ []) do
    %{group_id: group_id, wrong_answers: wrong_answers,
      user_id: @user_id, term_ids: term_ids}
    |> TestResult.new_changeset
    |> Repo.insert!
  end

  defp save_quiz_result(deck_id, wrong_answers) do
    %{deck_id: deck_id, wrong_answers: wrong_answers, user_id: @user_id}
    |> QuizResult.new_changeset
    |> Repo.insert!
  end

  test "for_deck_replace_term_with_language" do
    create_deck

    deck = create_deck
    term1 = create_term(deck)
    term2 = create_term(deck)
    translation2 = create_translation(term2)
    translation1 = create_translation(term1)

    terms = TermQuery.for_deck_replace_term_with_language(deck.id, SpanishTerm)

    [saved_term1, saved_term2] = Enum.sort(terms, &(&1.id < &2.id))
    assert saved_term1.term == translation1.term
    assert saved_term2.term == translation2.term
  end

  test "for_deck_replace_term_with_language only return term for deck" do
    create_deck |> create_term |> create_translation

    deck = create_deck
    term1 = create_term(deck)
    translation1 = create_translation(term1)

    [saved_term] = TermQuery.for_deck_replace_term_with_language(deck.id, SpanishTerm)

    assert saved_term.term == translation1.term
  end

  test "get language for the terms" do
    deck = create_deck
    term1 = create_term(deck)
    term2 = create_term(deck)
    translation2 = create_translation(term2)
    translation1 = create_translation(term1)

    terms = TermQuery.for_deck_with_language(deck.id, SpanishTerm)

    [saved_term1, saved_term2] = Enum.sort(terms, &(&1.id < &2.id))
    assert saved_term1.translation.id == translation1.id
    assert saved_term2.translation.id == translation2.id
  end

  test "get language for the terms when there is no language" do
    deck = create_deck
    create_term(deck)

    [term] = TermQuery.for_deck_with_language(deck.id, SpanishTerm)

    assert term.translation
  end


  test "for group with language" do
    group = create_group
    deck = create_deck(%{group_id: group.id})
    term1 = create_term(deck)
    term2 = create_term(deck)
    translation2 = create_translation(term2)
    translation1 = create_translation(term1)

    [t1, t2] = TermQuery.for_group_with_language(group.id, SpanishTerm)
                |> Enum.sort(&(&1.id < &2.id))

    assert t1.term == translation1.term
    assert t2.term == translation2.term
  end
end
