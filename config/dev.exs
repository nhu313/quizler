use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :quizler, Quizler.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]]


config :quizler, Quizler.Endpoint,
  secret_key_base: "HVRJ5qRDtE4tRzEL4Wy7EGSyd3ovxYWhPK4QbyzTp5tqMc9lU9FVrpLTo1OATmWE"

# Watch static and templates for browser reloading.
config :quizler, Quizler.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  redirect_uri: "http://localhost:4000/users/auth/google/callback"

# Configure your database
config :quizler, Quizler.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_USERNAME"),
  password: System.get_env("POSTGRES_PASSWORD"),
  database: "quizler_dev",
  hostname: "localhost",
  pool_size: 10
