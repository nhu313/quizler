defmodule Quizler.LegalTerms.Group1SpanishSeeder do
  alias Quizler.{Repo, Term, SpanishTerm}

  def execute do
    create("priv/repo/terms/group1_spanish.json")
  end

  def create(file_name) do
    file_name
    |> File.read!
    |> Poison.decode!
    |> Enum.map(&create_term/1)
  end

  defp create_term(params) do
    params
    |> Map.put("source_term_id", term_id(params))
    |> SpanishTerm.new_changeset
    |> Repo.insert!
  end

  defp term_id(params) do
    term = Repo.get_by(Term, term: params["english_term"]) || %{id: 0}
    term.id
  end


end

Quizler.LegalTerms.Group1SpanishSeeder.execute
