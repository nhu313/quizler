Quizler.SeederExamQuestionType.run
Quizler.SeederLanguage.run
Quizler.SeederTermSmall.run("priv/repo/terms/group1.json", "Legal Terms")
Quizler.SeederTermSmall.run("priv/repo/idioms_slang/idioms_small.json", "Idioms")
Quizler.SeederExam.run("priv/repo/exam/ny_legal.json", "NY Test - Legal Terms Only", "NYLEGAL")
