Quizler.SeederExamQuestionType.run
Quizler.SeederLanguage.run

Quizler.SeederTerm.run("priv/repo/terms/ny_criminal.json", "Criminal Court")
Quizler.SeederTerm.run("priv/repo/terms/ny_civil.json", "Civil Court")
Quizler.SeederTerm.run("priv/repo/terms/ny_family.json", "Family Court")
Quizler.SeederTerm.run("priv/repo/terms/ny_surrogate.json", "Surrogate Court")
Quizler.SeederTerm.run("priv/repo/terms/ny_general.json", "General Legal Term")

Quizler.SeederTerm.run("priv/repo/idioms_slang/idioms.json", "Idioms")
Quizler.SeederTerm.run("priv/repo/idioms_slang/idioms_uncommon.json", "Uncommon Idioms")

Quizler.SeederExam.run("priv/repo/exam/ny.json", "New York Sample Exam")
Quizler.SeederExam.run("priv/repo/exam/pa.json", "Pennsylvania Sample Exam")
