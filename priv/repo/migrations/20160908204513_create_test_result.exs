defmodule Quizler.Repo.Migrations.CreateTestResult do
  use Ecto.Migration

  def change do
    create table(:test_results) do
      add :user_id, :integer
      add :group_id, :integer
      add :wrong_answers, {:array, :integer}

      timestamps()
    end

    create index(:test_results, [:user_id, :group_id])
  end
end
