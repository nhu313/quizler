defmodule Quizler.Repo.Migrations.CreateExamQuestion do
  use Ecto.Migration

  def change do
    create table(:exam_questions) do
      add :exam_id, :integer
      add :question, :string
      add :answer, :string
      add :choices, {:array, :string}
      add :type_id, :integer

      timestamps()
    end

    create index(:exam_questions, [:exam_id])
  end
end
