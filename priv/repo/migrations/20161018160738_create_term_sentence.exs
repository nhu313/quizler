defmodule Quizler.Repo.Migrations.CreateTermSentence do
  use Ecto.Migration

  def change do
    create table(:term_sentences) do
      add :term_id, :integer
      add :sentence, :string, size: 500

      timestamps()
    end

    create index(:term_sentences, [:term_id])
  end
end
