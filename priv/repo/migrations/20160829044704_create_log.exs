defmodule Quizler.Repo.Migrations.CreateLog do
  use Ecto.Migration

  def change do
    create table(:logs) do
      add :user_id, :integer
      add :path, :string

      timestamps()
    end

  end
end
