defmodule Quizler.Repo.Migrations.CreateUserTestGroup do
  use Ecto.Migration

  def change do
    create table(:users_test_groups) do
      add :user_id, :integer
      add :group_id, :integer

      timestamps()
    end

  end
end
