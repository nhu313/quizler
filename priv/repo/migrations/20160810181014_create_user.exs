defmodule Quizler.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :email, :string, null: false
      add :photo_url, :string
      add :password_hash, :string
      add :source, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
