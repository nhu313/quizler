defmodule Quizler.Repo.Migrations.CreateExamResult do
  use Ecto.Migration

  def change do
    create table(:exam_results) do
      add :user_id, :integer
      add :exam_id, :integer
      add :wrong_answers, {:array, :integer}

      timestamps()
    end

    create index(:exam_results, [:user_id, :exam_id])
  end
end
