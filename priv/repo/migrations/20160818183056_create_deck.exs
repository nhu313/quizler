defmodule Quizler.Repo.Migrations.CreateDeck do
  use Ecto.Migration

  def change do
    create table(:decks) do
      add :group_id, :integer
      add :name, :string
      add :image, :string
      add :step, :integer

      timestamps()
    end

    create index(:decks, [:group_id])
  end
end
