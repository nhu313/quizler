defmodule Quizler.Repo.Migrations.CreateExam do
  use Ecto.Migration

  def change do
    create table(:exams) do
      add :name, :string
      add :source_url, :string

      timestamps()
    end

  end
end
