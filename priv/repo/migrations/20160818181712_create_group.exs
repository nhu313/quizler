defmodule Quizler.Repo.Migrations.CreateGroup do
  use Ecto.Migration

  def change do
    create table(:groups) do
      add :name, :string
      add :user_id, :string

      timestamps()
    end

    create index(:groups, [:user_id])
  end
end
