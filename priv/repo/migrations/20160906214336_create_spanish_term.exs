defmodule Quizler.Repo.Migrations.CreateSpanishTerm do
  use Ecto.Migration

  def change do
    create table(:spanish_terms) do
      add :source_term_id, :integer
      add :term, :string
      add :definition, :string

      timestamps()
    end

    create index(:spanish_terms, [:source_term_id])
  end
end
