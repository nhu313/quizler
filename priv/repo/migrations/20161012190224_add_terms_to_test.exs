defmodule Quizler.Repo.Migrations.AddTermsToTest do
  use Ecto.Migration

  def change do
    alter table(:test_results) do
      add :term_ids, {:array, :integer}, default: []
    end
  end
end
