defmodule Quizler.Repo.Migrations.AddCodeToGroup do
  use Ecto.Migration

  def change do
    alter table(:exams) do
      add :code, :string
    end
  end
end
