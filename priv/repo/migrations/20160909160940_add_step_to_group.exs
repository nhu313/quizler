defmodule Quizler.Repo.Migrations.AddStepToGroup do
  use Ecto.Migration

  def change do
    alter table(:groups) do
      add :step, :integer, default: 0
    end
  end
end
