defmodule Quizler.Repo.Migrations.DeleteDoneDeck do
  use Ecto.Migration

  def change do
    drop_if_exists index(:deck_done, [:user_id, :deck_id])
    drop_if_exists table(:deck_done)
  end
end
