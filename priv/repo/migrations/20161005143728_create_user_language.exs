defmodule Quizler.Repo.Migrations.CreateUserLanguage do
  use Ecto.Migration

  def change do
    create table(:users_languages) do
      add :user_id, :integer
      add :language_id, :integer

      timestamps()
    end

    create index(:users_languages, [:user_id])
    create index(:users_languages, [:language_id])
  end
end
