defmodule Quizler.Repo.Migrations.CreateTerm do
  use Ecto.Migration

  def change do
    create table(:terms) do
      add :term, :string
      add :definition, :string, size: 1024
      add :image, :string
      add :deck_id, :integer

      timestamps()
    end

    create index(:terms, [:term])
    create index(:terms, [:deck_id])
  end
end
