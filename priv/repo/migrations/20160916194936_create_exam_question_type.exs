defmodule Quizler.Repo.Migrations.CreateExamQuestionType do
  use Ecto.Migration

  def change do
    create table(:exam_question_types) do
      add :name, :string
      add :instructions, :string, size: 2000

      timestamps()
    end

  end
end
