defmodule Quizler.Repo.Migrations.CreateQuizResult do
  use Ecto.Migration

  def change do
    create table(:quiz_results) do
      add :user_id, :integer
      add :deck_id, :integer
      add :wrong_answers, {:array, :integer}

      timestamps()
    end

    create index(:quiz_results, [:user_id, :deck_id])
  end
end
