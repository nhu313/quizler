defmodule Quizler.SeedTermSentence do
  alias Quizler.{Repo, Term, TermSentence}

  def run(file_name) do
    file_name
    |> File.read!
    |> Poison.decode!
    |> Enum.map(&create_sentence/1)
  end

  defp create_sentence(term) do
    db_term = Repo.get_by(Term, term: term["term"])
    if db_term do
      term["sentences"]
      |> Enum.map(&(create_sentence(&1, db_term)))
    else
      IO.inspect "Can't find term"
      IO.inspect term
    end
  end

  defp create_sentence(sentence, term) do
    %{term_id: term.id, sentence: sentence}
    |> TermSentence.new_changeset
    |> Repo.insert!
  end
end

Quizler.SeedTermSentence.run("priv/repo/idioms_slang/idioms.json")
Quizler.SeedTermSentence.run("priv/repo/idioms_slang/idioms_uncommon.json")
