defmodule Quizler.SharedController do

  def user(conn) do
    Plug.Conn.get_session(conn, :user)
  end
end
