defmodule Quizler.SpanishTermController do
  use Quizler.Web, :controller

  alias Quizler.SpanishTerm
  plug Quizler.Admin


  def index(conn, _params) do
    spanish_terms = Repo.all(SpanishTerm)
    render(conn, "index.html", spanish_terms: spanish_terms)
  end

  def new(conn, _params) do
    changeset = SpanishTerm.changeset(%SpanishTerm{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"spanish_term" => spanish_term_params}) do
    changeset = SpanishTerm.changeset(%SpanishTerm{}, spanish_term_params)

    case Repo.insert(changeset) do
      {:ok, _spanish_term} ->
        conn
        |> put_flash(:info, "Spanish term created successfully.")
        |> redirect(to: spanish_term_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    spanish_term = Repo.get!(SpanishTerm, id)
    render(conn, "show.html", spanish_term: spanish_term)
  end

  def edit(conn, %{"id" => id}) do
    spanish_term = Repo.get!(SpanishTerm, id)
    changeset = SpanishTerm.changeset(spanish_term)
    render(conn, "edit.html", spanish_term: spanish_term, changeset: changeset)
  end

  def update(conn, %{"id" => id, "spanish_term" => spanish_term_params}) do
    spanish_term = Repo.get!(SpanishTerm, id)
    changeset = SpanishTerm.changeset(spanish_term, spanish_term_params)

    case Repo.update(changeset) do
      {:ok, spanish_term} ->
        conn
        |> put_flash(:info, "Spanish term updated successfully.")
        |> redirect(to: spanish_term_path(conn, :show, spanish_term))
      {:error, changeset} ->
        render(conn, "edit.html", spanish_term: spanish_term, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    spanish_term = Repo.get!(SpanishTerm, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(spanish_term)

    conn
    |> put_flash(:info, "Spanish term deleted successfully.")
    |> redirect(to: spanish_term_path(conn, :index))
  end
end
