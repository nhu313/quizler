defmodule Quizler.PageController do
  use Quizler.Web, :controller

  def index(conn, params) do
    if Quizler.Authorizer.logged_in?(conn) do
      redirect(conn, to: group_path(conn, :index))
    else
      about(conn, params)
    end
  end

  def about(conn, _params) do
    conn
    |> put_layout(false)
    |> render("about.html")
  end
end
