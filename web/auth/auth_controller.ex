defmodule Quizler.AuthController do
  use Quizler.Web, :controller
  plug Ueberauth

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate.")
    |> redirect(to: "/")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    case Quizler.User.Saver.create_or_find(auth) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Happy to have you here!")
        |> Quizler.Authorizer.login(user)
      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: "/")
    end
  end
end
