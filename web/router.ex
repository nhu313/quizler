defmodule Quizler.Router do
  use Quizler.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Quizler.Logger, repo: Quizler.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/users", Quizler do
    pipe_through :browser

    get "/login", SessionController, :new
    post "/login", SessionController, :create

    get "/", UserController, :index
    get "/signup", UserController, :new
    post "/", UserController, :create
    get "/:id", UserController, :show
    delete "/", UserController, :delete
    
    get "/auth/:provider", AuthController, :request
    get "/auth/:provider/callback", AuthController, :callback

    delete "/logout", SessionController, :delete
  end

  scope "/", Quizler do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/about", PageController, :about

    get "/training", GroupController, :index
    get "/decks/:id/terms/json", DeckController, :terms_json
    get "/decks/:id/terms", DeckController, :terms
    get "/decks/:deck_id/quiz/done", DeckController, :done_quiz
    post "/decks/:deck_id/quiz/done", DeckController, :done_quiz
    get "/decks/:id/quiz", DeckController, :quiz
    get "/decks/:id/quiz_items", DeckController, :quiz_items
    get "/decks/:id/done", DeckController, :done_flashcard
    get "/decks/:deck_id/games/match/cards", Deck.GameController, :game_match_cards
    get "/decks/:deck_id/games/match", Deck.GameController, :game_match
    get "/decks/:deck_id/games/match/done", Deck.GameController, :game_match_done

    get "/groups/all", GroupController, :all
    get "/groups/:id/test", GroupController, :test
    get "/groups/:id/test_items", GroupController, :test_items
    get "/groups/:id/terms", GroupController, :terms
    get "/groups/test_results", GroupController, :test_results
    delete "/groups/test_results/:test_result_id", GroupController, :delete_test_result

    post "/groups/:group_id/test/done", GroupController, :test_done
    resources "/terms", TermController
    resources "/groups", GroupController
    resources "/decks", DeckController
    resources "/logs", LogController

    resources "/test_groups", UserTestGroupController

    resources "/spanish_terms", SpanishTermController

    get "/exams/code/:code", ExamController, :code
    resources "/exams/questions", ExamQuestionController
    get "/exams/results", ExamController, :results
    delete "/exams/results/:result_id", ExamController, :delete_result
    get "/exams/:id/questions", ExamController, :questions
    post "/exams/:exam_id/done", ExamController, :done
    resources "/exams/question_types", ExamQuestionTypeController

    resources "/exams", ExamController
    resources "/user_languages", UserLanguageController
    resources "/languages", LanguageController
    resources "/term_sentences", TermSentenceController
  end


  # Other scopes may use custom stacks.
  # scope "/api", Quizler do
  #   pipe_through :api
  # end
end
