defmodule Quizler.ExamQuestionController do
  use Quizler.Web, :controller

  alias Quizler.ExamQuestion
  plug Quizler.Admin

  def index(conn, _params) do
    exam_questions = Repo.all(ExamQuestion)
    render(conn, "index.html", exam_questions: exam_questions)
  end

  def new(conn, _params) do
    changeset = ExamQuestion.changeset(%ExamQuestion{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"exam_question" => exam_question_params}) do
    changeset = ExamQuestion.changeset(%ExamQuestion{}, exam_question_params)

    case Repo.insert(changeset) do
      {:ok, _exam_question} ->
        conn
        |> put_flash(:info, "Exam question created successfully.")
        |> redirect(to: exam_question_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    exam_question = Repo.get!(ExamQuestion, id)
    render(conn, "show.html", exam_question: exam_question)
  end

  def edit(conn, %{"id" => id}) do
    exam_question = Repo.get!(ExamQuestion, id)
    changeset = ExamQuestion.changeset(exam_question)
    render(conn, "edit.html", exam_question: exam_question, changeset: changeset)
  end

  def update(conn, %{"id" => id, "exam_question" => exam_question_params}) do
    exam_question = Repo.get!(ExamQuestion, id)
    changeset = ExamQuestion.changeset(exam_question, exam_question_params)

    case Repo.update(changeset) do
      {:ok, exam_question} ->
        conn
        |> put_flash(:info, "Exam question updated successfully.")
        |> redirect(to: exam_question_path(conn, :show, exam_question))
      {:error, changeset} ->
        render(conn, "edit.html", exam_question: exam_question, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    exam_question = Repo.get!(ExamQuestion, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(exam_question)

    conn
    |> put_flash(:info, "Exam question deleted successfully.")
    |> redirect(to: exam_question_path(conn, :index))
  end
end
