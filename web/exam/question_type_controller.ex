defmodule Quizler.ExamQuestionTypeController do
  use Quizler.Web, :controller

  alias Quizler.ExamQuestionType
  plug Quizler.Admin

  def index(conn, _params) do
    exam_question_types = Repo.all(ExamQuestionType)
    render(conn, "index.html", exam_question_types: exam_question_types)
  end

  def new(conn, _params) do
    changeset = ExamQuestionType.changeset(%ExamQuestionType{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"exam_question_type" => exam_question_type_params}) do
    changeset = ExamQuestionType.changeset(%ExamQuestionType{}, exam_question_type_params)

    case Repo.insert(changeset) do
      {:ok, _exam_question_type} ->
        conn
        |> put_flash(:info, "Exam question type created successfully.")
        |> redirect(to: exam_question_type_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    exam_question_type = Repo.get!(ExamQuestionType, id)
    render(conn, "show.html", exam_question_type: exam_question_type)
  end

  def edit(conn, %{"id" => id}) do
    exam_question_type = Repo.get!(ExamQuestionType, id)
    changeset = ExamQuestionType.changeset(exam_question_type)
    render(conn, "edit.html", exam_question_type: exam_question_type, changeset: changeset)
  end

  def update(conn, %{"id" => id, "exam_question_type" => exam_question_type_params}) do
    exam_question_type = Repo.get!(ExamQuestionType, id)
    changeset = ExamQuestionType.changeset(exam_question_type, exam_question_type_params)

    case Repo.update(changeset) do
      {:ok, exam_question_type} ->
        conn
        |> put_flash(:info, "Exam question type updated successfully.")
        |> redirect(to: exam_question_type_path(conn, :show, exam_question_type))
      {:error, changeset} ->
        render(conn, "edit.html", exam_question_type: exam_question_type, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    exam_question_type = Repo.get!(ExamQuestionType, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(exam_question_type)

    conn
    |> put_flash(:info, "Exam question type deleted successfully.")
    |> redirect(to: exam_question_type_path(conn, :index))
  end
end
