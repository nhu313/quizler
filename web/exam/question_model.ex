defmodule Quizler.ExamQuestion do
  use Quizler.Web, :model

  @derive {Poison.Encoder, only: [:id, :exam_id, :question, :answer, :choices]}
  schema "exam_questions" do
    field :exam_id, :integer
    field :question, :string
    field :answer, :string
    field :choices, {:array, :string}
    field :type_id, :integer
    field :user_answer, :string, virtual: true

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:exam_id, :question, :answer, :choices, :type_id])
    |> validate_required([:exam_id, :question, :answer, :choices])
  end

  def new_changeset(params) do
    %Quizler.ExamQuestion{}
    |> cast(params, [:exam_id, :question, :answer, :choices, :type_id])
    |> validate_required([:exam_id, :question, :answer, :choices])
  end
end
