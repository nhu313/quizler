defmodule Quizler.ExamQuery do
  import Ecto.Query
  alias Quizler.{Repo, Exam, ExamQuestionType, ExamResult}

  def all do
    Repo.all(Exam)
  end

  def with_questions_and_answers(id, user_answers, user_id) do
    exam = with_questions(id)
    types = get_question_types

    grouped_questions = exam.questions
                        |> Enum.map(&(add_user_answer(&1, user_answers)))
                        |> Enum.group_by(&(&1.type_id))
                        |> Enum.map(&(add_question_type(&1, types)))

    exam
    |> Map.put(:grouped_questions, grouped_questions)
    |> Map.put(:results, exam_results(user_id, id))
  end

  defp exam_results(user_id, exam_id) do
    query = from result in ExamResult,
            where: result.user_id == ^user_id and result.exam_id == ^exam_id,
            order_by: [desc: result.inserted_at]

    Repo.all(query)
  end

  defp add_user_answer(question, user_answers) do
    answer = Map.get(user_answers, Integer.to_string(question.id))
    Map.put(question, :user_answer, answer)
  end

  def with_questions_and_instructions(id) do
    exam = with_questions(id)
    types = get_question_types

    grouped_questions = exam.questions
                        |> Enum.map(&add_answer_to_choices/1)
                        |> Enum.group_by(&(&1.type_id))
                        |> Enum.map(&(add_question_type(&1, types)))

    Map.put(exam, :grouped_questions, grouped_questions)
  end

  defp add_answer_to_choices(question) do
    choices = [question.answer | question.choices] |> Enum.shuffle
    Map.put(question, :choices, choices)
  end

  @no_type %{name: "", instructions: ""}
  defp add_question_type({type_id, questions}, types) do
    type = types[type_id] || @no_type
    %{name: type.name || "",
      instructions: type.instructions || "",
      questions: questions}
  end

  defp get_question_types() do
    func = fn(x, acc) -> Map.put(acc, x.id, x) end
    ExamQuestionType |> Repo.all |> Enum.reduce(%{}, func)
  end

  defp with_questions(id) do
    query = from exam in Exam,
            where: exam.id == ^id,
            preload: [:questions]

    Repo.one(query)
  end
end
