defmodule Quizler.ExamQuestionType do
  use Quizler.Web, :model

  schema "exam_question_types" do
    field :name, :string, default: ""
    field :instructions, :string, default: ""

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :instructions])
    |> validate_required([:name])
  end

  def new_changeset(params) do
    %Quizler.ExamQuestionType{}
    |> cast(params, [:name, :instructions])
    |> validate_required([:name])
  end
end
