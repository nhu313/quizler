defmodule Quizler.ExamQuestionQuery do
  import Ecto.Query

  alias Quizler.{Repo, ExamQuestion}

  def exam(exam_id) do
    query = from q in ExamQuestion,
            where: q.exam_id == ^exam_id

    Repo.all(query)
  end

end
