defmodule Quizler.Exam do
  use Quizler.Web, :model

  @derive {Poison.Encoder, only: [:id, :name, :questions]}
  schema "exams" do
    field :name, :string
    field :source_url, :string
    field :code, :string

    has_many :questions, Quizler.ExamQuestion, on_delete: :delete_all
    has_many :results, Quizler.ExamResult

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :source_url, :code])
    |> validate_required([:name])
  end

  def new_changeset(params) do
    %Quizler.Exam{}
    |> cast(params, [:name, :source_url, :code])
    |> validate_required([:name])
  end
end
