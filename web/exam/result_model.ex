defmodule Quizler.ExamResult do
  use Quizler.Web, :model

  schema "exam_results" do
    field :user_id, :integer
    field :exam_id, :integer
    field :wrong_answers, {:array, :integer}

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :exam_id, :wrong_answers])
    |> validate_required([:user_id, :exam_id])
  end

  def new_changeset(params) do
    %Quizler.ExamResult{}
    |> cast(params, [:user_id, :exam_id, :wrong_answers])
    |> validate_required([:user_id, :exam_id])
  end
end
