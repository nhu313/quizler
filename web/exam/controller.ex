defmodule Quizler.ExamController do
  use Quizler.Web, :controller

  alias Quizler.{Exam, ExamQuery, ResultSaver, ExamResult}
  plug Quizler.Authorizer
  plug Quizler.Admin when action in [:new, :create, :edit, :update, :delete]
  plug :scrub_params, "exam" when action in [:create, :update]
  plug :scrub_params, "code" when action in [:code]

  def code(conn, %{"code" => code}) do
    exam = Repo.get_by!(Exam, code: code)
    render(conn, "show.html", exam: exam)
  end

  def done(conn, params) do
    user_id = user(conn).id
    ResultSaver.create_for_exam(params, user_id)
    exam = ExamQuery.with_questions_and_answers(params["exam_id"], params["answers"], user_id)
    render(conn, "done.html", exam: exam)
  end

  def questions(conn, %{"id" => id}) do
    exam = ExamQuery.with_questions_and_instructions(id)
    render(conn, "questions.json", exam: exam)
  end

  def index(conn, _params) do
    exams = Repo.all(Exam)
    render(conn, "index.html", exams: exams)
  end

  def new(conn, _params) do
    changeset = Exam.changeset(%Exam{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"exam" => exam_params}) do
    changeset = Exam.changeset(%Exam{}, exam_params)

    case Repo.insert(changeset) do
      {:ok, _exam} ->
        conn
        |> put_flash(:info, "Exam created successfully.")
        |> redirect(to: exam_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    exam = Repo.get!(Exam, id)
    render(conn, "show.html", exam: exam)
  end

  def edit(conn, %{"id" => id}) do
    exam = Repo.get!(Exam, id)
    changeset = Exam.changeset(exam)
    render(conn, "edit.html", exam: exam, changeset: changeset)
  end

  def update(conn, %{"id" => id, "exam" => exam_params}) do
    exam = Repo.get!(Exam, id)
    changeset = Exam.changeset(exam, exam_params)

    case Repo.update(changeset) do
      {:ok, exam} ->
        conn
        |> put_flash(:info, "Exam updated successfully.")
        |> redirect(to: exam_path(conn, :show, exam))
      {:error, changeset} ->
        render(conn, "edit.html", exam: exam, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    exam = Repo.get!(Exam, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(exam)

    conn
    |> put_flash(:info, "Exam deleted successfully.")
    |> redirect(to: exam_path(conn, :index))
  end

  def results(conn, _) do
    results = Repo.all(ExamResult)
    render(conn, "results.html", results: results)
  end

  def delete_result(conn, %{"result_id" => id}) do
    result = Repo.get!(ExamResult, id)

    Repo.delete(result)

    conn
    |> put_flash(:info, "Exam result deleted successfully.")
    |> redirect(to: exam_path(conn, :results))
  end
end
