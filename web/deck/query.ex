defmodule Quizler.DeckQuery do
  import Ecto.Query

  alias Quizler.{Repo, Deck}

  def id(id) do
    Repo.get!(Deck, id)
  end

  def with_group(deck_id) do
    query = from deck in Deck,
            where: deck.id == ^deck_id,
            preload: [:group]

    Repo.one(query)
  end

  def for_group(group_id) do
    query = from deck in Deck,
            where: deck.group_id == ^group_id

    Repo.all(query)
  end

  def with_terms(deck_id) do
    query = from deck in Deck,
            where: deck.id == ^deck_id,
            preload: [:terms]

    Repo.one(query)
  end

  def next(current_deck) do
    query = from deck in Deck,
            where: deck.step > ^current_deck.step,
            limit: 1,
            order_by: [asc: :step]

    Repo.one(query)
  end
end
