defmodule Quizler.Deck do
  use Quizler.Web, :model

  schema "decks" do
    field :name, :string
    field :image, :string
    field :step, :integer, default: 0

    has_many :terms, Quizler.Term, on_delete: :delete_all
    belongs_to :group, Quizler.Group

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:group_id, :name, :image])
    |> validate_required([:group_id, :name])
  end

  def new_changeset(params) do
    %Quizler.Deck{}
    |> cast(params, [:group_id, :name, :image, :step])
    |> validate_required([:group_id, :name])
  end
end
