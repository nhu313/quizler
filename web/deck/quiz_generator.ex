defmodule Quizler.Deck.QuizGenerator do

  def create(terms) do
    choices = Enum.map(terms, &(&1.term))
    Enum.map(terms, &(create_question(&1, choices)))
    |> Enum.shuffle
  end

  defp create_question(term, all_choices) do
    choices = all_choices |> Enum.shuffle |> Enum.take(4)
    if Enum.member?(choices, term.term) do
      create_item(term, choices)
    else
      create_item(term, add_answer_to_choices(term, choices))
    end
  end

  defp add_answer_to_choices(term, choices) do
    choices
      |> Enum.drop(1)
      |> Enum.concat([term.term])
      |> Enum.shuffle
  end

  defp create_item(term, choices) do
    %{question: term.definition,
       answer: term.term,
       term_id: term.id,
       choices: choices}
  end

end
