defmodule Quizler.DeckController do
  use Quizler.Web, :controller

  alias Quizler.{Deck, TermQuery, DeckQuery, Deck.QuizGenerator, ResultSaver, SpanishTerm, UserTestGroup.Helper}
  plug Quizler.Authorizer
  plug Quizler.Admin when action in [:new, :create, :edit, :update, :delete]

  def show(conn, params) do
    show_version(conn, Helper.add_test_group(user(conn).id, params))
  end

  def show_version(conn, %{"id" => id, "test_group" => 2}) do
    deck = DeckQuery.id(id)
    render(conn, "show_with_translation.html", deck: deck)
  end

  def show_version(conn, %{"id" => id}) do
    deck = DeckQuery.id(id)
    render(conn, "show_flashcard.html", deck: deck)
  end

  def spanish_json(conn, %{"id" => id}) do
    terms = TermQuery.for_deck_with_language(id, SpanishTerm)
    render(conn, "terms.json", terms: terms)
  end

  def done_quiz(conn, params) do
    ResultSaver.create_quiz(params, user(conn).id)
    deck = DeckQuery.id(params["deck_id"])
    next_deck = DeckQuery.next(deck)
    render(conn, "quiz_done.html", deck: deck, next_deck: next_deck)
  end

  def done_flashcard(conn, %{"id" => id}) do
    render(conn, "done.html", deck: DeckQuery.id(id))
  end

  def quiz(conn, params) do
    quiz_version(conn, Helper.add_test_group(user(conn).id, params))
  end

  def quiz_version(conn, %{"id" => id, "test_group" => 2}) do
    deck = DeckQuery.with_group(id)
    render(conn, "quiz.html", deck: deck)
  end

  def quiz_version(conn, %{"id" => id}) do
    deck = DeckQuery.with_group(id)
    render(conn, "quiz.html", deck: deck)
  end

  def quiz_items(conn, params) do
    quiz_items_version(conn, Helper.add_test_group(user(conn).id, params))
  end

  def quiz_items_version(conn, %{"id" => id, "test_group" => 2}) do
    items = TermQuery.for_deck_replace_term_with_language(id, SpanishTerm)
            |> QuizGenerator.create

    render(conn, "items.json", items: items)
  end

  def quiz_items_version(conn, %{"id" => id}) do
    items = TermQuery.for_deck(id)
            |> QuizGenerator.create
    render(conn, "items.json", items: items)
  end

  def terms(conn, %{"id" => id}) do
    deck = DeckQuery.with_terms(id)
    render(conn, "terms.html", deck: deck)
  end

  def terms_json(conn, %{"id" => id}) do
    terms = TermQuery.for_deck_with_language(id, SpanishTerm) |> Enum.shuffle
    render(conn, "terms.json", terms: terms)
  end

  def index(conn, _params) do
    decks = Repo.all(Deck)
    render(conn, "index.html", decks: decks)
  end

  def new(conn, _params) do
    changeset = Deck.changeset(%Deck{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"deck" => deck_params}) do
    changeset = Deck.changeset(%Deck{}, deck_params)

    case Repo.insert(changeset) do
      {:ok, _deck} ->
        conn
        |> put_flash(:info, "Deck created successfully.")
        |> redirect(to: deck_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    deck = Repo.get!(Deck, id)
    changeset = Deck.changeset(deck)
    render(conn, "edit.html", deck: deck, changeset: changeset)
  end

  def update(conn, %{"id" => id, "deck" => deck_params}) do
    deck = Repo.get!(Deck, id)
    changeset = Deck.changeset(deck, deck_params)

    case Repo.update(changeset) do
      {:ok, deck} ->
        conn
        |> put_flash(:info, "Deck updated successfully.")
        |> redirect(to: deck_path(conn, :show, deck))
      {:error, changeset} ->
        render(conn, "edit.html", deck: deck, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    deck = Repo.get!(Deck, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(deck)

    conn
    |> put_flash(:info, "Deck deleted successfully.")
    |> redirect(to: deck_path(conn, :index))
  end
end
