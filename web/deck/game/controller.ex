defmodule Quizler.Deck.GameController do
  use Quizler.Web, :controller
  alias Quizler.{TermQuery, Deck.GameFormatter, DeckQuery}
  plug Quizler.Authorizer

  def game_match(conn, %{"deck_id" => deck_id}) do
    render(conn, "match.html", deck: DeckQuery.id(deck_id))
  end

  def game_match_cards(conn, %{"deck_id" => deck_id}) do
    cards = deck_id
            |> TermQuery.for_deck
            |> Enum.shuffle
            |> Enum.take(6)
            |> GameFormatter.cards_for_matching
            |> Enum.shuffle
    render(conn, "cards_for_matching.json", cards: cards)
  end

  def game_match_done(conn, %{"deck_id" => deck_id}) do
    deck = DeckQuery.id(deck_id)

    render(conn, "match_done.html", deck: deck)
  end
end
