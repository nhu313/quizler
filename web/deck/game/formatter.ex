defmodule Quizler.Deck.GameFormatter do

  def cards_for_matching(terms) do
    terms
    |> Enum.map(&create_card/1)
    |> List.flatten
  end

  defp create_card(term) do
    [%{term_id: term.id, name: term.term, status: ""},
     %{term_id: term.id, name: term.definition, status: ""}]
  end



end
