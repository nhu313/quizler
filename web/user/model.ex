defmodule Quizler.User do
  use Quizler.Web, :model

  schema "users" do
    field :name, :string
    field :email, :string
    field :photo_url, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :password_hash, :string
    field :source, :string

    many_to_many :languages, Quizler.Language, join_through: "users_languages"

    timestamps()
  end

  def empty_changeset(params \\ %Quizler.User{}) do
    params
    |> cast(%{}, [])
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def new_changeset(params) do
    %Quizler.User{}
    |> cast(params, [:name, :email, :photo_url])
    |> validate_required([:name, :email])
  end

  def registration_changeset(params) do
    %Quizler.User{}
    |> cast(params, [:name, :email, :password, :password_confirmation, :photo_url])
    |> validate_required([:name, :email, :password, :password_confirmation])
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password, required: true, message: "Confirmation does not match password")
    |> unique_constraint(:email)
    |> add_hashed_password
  end

  defp add_hashed_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))
    _ ->
      changeset
    end
  end
end
