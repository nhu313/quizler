defmodule Quizler.UserQuery do
  import Ecto.Query
  alias Quizler.{Repo, User}

  def email(email) do
    Repo.get_by(User, email: email)
  end

  def all(user_id) do
    query = from user in User,
      where: user.id == ^user_id,
      preload: [:languages]

    Repo.one(query)
  end

  def id!(user_id) do
    Repo.get!(User, user_id)
  end

end
