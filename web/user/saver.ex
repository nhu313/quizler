defmodule Quizler.User.Saver do
    alias Quizler.{Repo, UserQuery, User}

    def create_or_find(auth) do
      info = auth.info

      params = %{email: info.email,
                 name: name(info),
                 photo_url: remove_size(info.image)}

      user = UserQuery.email(params.email)
      if user do
        {:ok, user}
      else
        save(params)
      end
    end

    defp save(params) do
      params
      |> User.new_changeset
      |> Repo.insert
    end

    defp remove_size(url) do
      [value | _] = String.split(url, "?")
      value
    end

    defp name(info) do
      cond do
       info.first_name && info.first_name != "" ->
        info.first_name
       info.name && info.name != "" ->
        info.name
       true ->
        [username | _] = String.split(info.email, "@")
        username
      end
    end
end
