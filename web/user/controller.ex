defmodule Quizler.UserController do
  use Quizler.Web, :controller
  alias Quizler.{User, Language, LanguageSaver, UserQuery}

  plug Quizler.Admin when action in [:show]

  def new(conn, _) do
    changeset = %Quizler.User{} |> Map.put(:languages, []) |> User.empty_changeset
    render_form(conn, changeset)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.registration_changeset(user_params)
    case Repo.insert(changeset) do
      {:ok, user} ->
        LanguageSaver.save_user_languages(user, user_params["languages"])
        conn
        |> put_flash(:info, "User created successfully.")
        |> Quizler.Authorizer.login(user)
      {:error, changeset} ->
        render_form(conn, changeset)
    end
  end

  defp render_form(conn, changeset) do
    languages = Repo.all(Language) |> Enum.map(&({&1.name, &1.id}))
    render(conn, "new.html", changeset: changeset, languages: languages)
  end

  def show(conn, %{"id" => id}) do
    user = UserQuery.all(id)
    render(conn, "show.html", user: user)
  end

  def index(conn, _) do
    users = Repo.all(User)
    render(conn, "index.html", users: users)
  end

  def delete(conn, %{"id" => id}) do
    user = Repo.get!(User, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: user_path(conn, :index))
  end

end
