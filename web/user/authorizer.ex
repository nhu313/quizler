defmodule Quizler.Authorizer do
  import Plug.Conn

  def init(opts \\ []) do
    opts
  end

  def call(conn, _opts) do
    if logged_in?(conn) do
      conn
    else
      conn
      |> put_session(:original_path, conn.request_path)
      |> Phoenix.Controller.redirect(to: "/users/login")
    end
  end

  def logged_in?(conn) do
    get_session(conn, :user)
  end

  def login(conn, user) do
    conn
    |> delete_session(:original_path)
    |> put_session(:user, basic_user(user))
    |> configure_session(renew: true)
    |> Phoenix.Controller.redirect(to: redirect_path(conn))
    |> halt
  end

  defp basic_user(user) do
    %{name: user.name,
      photo_url: user.photo_url,
      id: user.id}
  end

  defp redirect_path(conn) do
    path = get_session(conn, :original_path)
    if !path || path == "/users/login" || path == "/users/signup" do
      "/"
    else
      path
    end
  end
end
