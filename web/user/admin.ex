defmodule Quizler.Admin do
  import Plug.Conn
  alias Quizler.{UserQuery}

  @admin 1
  def init(role \\ :admin) do
    @admin
  end

  def call(conn, _) do
    if admin?(conn) do
      conn
    else
      not_found(conn)
    end
  end

  def admin?(conn) do
    with %{id: user_id} <- get_session(conn, :user),
         db_user <- UserQuery.id!(user_id) do
           admin_emails = Application.get_env(:quizler, :users)[:admin_emails]
           if String.contains?(admin_emails, db_user.email) do
              true
           else
             false
           end
    else
      _ -> false
    end
  end

  defp not_found(conn) do
    conn
    |> Phoenix.Controller.put_flash(:error, "Not found.")
    |> Phoenix.Controller.redirect(to: "/")
    |> halt
  end
end
