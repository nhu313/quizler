defmodule Quizler.SessionController do
    use Quizler.Web, :controller
    plug :scrub_params, "session" when action in [:create]

    def new(conn, _params) do
      render(conn, "new.html")
    end

    def create(conn,  %{"session" => params}) do
      case user_by_email(params) do
        {:ok, user} ->
          conn
          |> Quizler.Authorizer.login(user)
        {:error, :provider} ->
            conn
            |> put_flash(:error, "Please login with your Google account")
            |> render("new.html")
        _ ->
          conn
          |> put_flash(:error, "Invalid email/password combination")
          |> render("new.html")
      end
    end

    defp user_by_email(params) do
      user = Repo.get_by(Quizler.User, email: params["email"])
      cond do
        user && (user.password_hash == nil) ->
          {:error, :provider}
        user && Comeonin.Bcrypt.checkpw(params["password"], user.password_hash) ->
          {:ok, user}
        true ->
          {:error, :not_found}
      end
    end

    def delete(conn, _) do
      conn
      |> configure_session(drop: true)
      |> put_flash(:info, "You are logged out!")
      |> redirect(to: "/")
    end
end
