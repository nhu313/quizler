"use strict";
export var DeckGameMatch = {
  run: function(vueAppName, cards_url, done_url) {
    $.get( cards_url )
    .done(function( cards ) {
      var empty_card = {};

      new Vue({
        el: vueAppName,
        data: {
          cards: cards,
          choice1: empty_card,
          choice2: empty_card,
          done_url: done_url
        },
        methods: {
          select: function(card) {
            if(card.status == "") {
              markChoice(this, card, empty_card);
            }
          },
        },
      }); //end of vue
    });//end of get done
    $('.loading').hide();
    $('.content').show();
  }
}

function markChoice(vue, card, empty_card) {
  if (vue.choice1 == empty_card) {
    card.status = "selected";
    vue.choice1 = card;
  } else {
    vue.choice2 = card;
    if (same(vue.choice1, vue.choice2)) {
      setStatus(vue, "good pulse", "done")
    } else {
      setStatus(vue, "bad shake", "")
    }
    reset(vue, empty_card);
  }
}

function done(cards) {
  for (var i = 0; i < cards.length; i++) {
    var card = cards[i];
    if (card.status != "done") {
      return false;
    }
  }
  return true;
}

function setStatus(vue, temp_status, final_status) {
  var card1 = vue.choice1;
  var card2 = vue.choice2;

  card1.status = temp_status;
  card2.status = temp_status;

  setTimeout(() => {
     card1.status = final_status;
     card2.status = final_status;

     if (final_status == "done" && done(vue.cards)) {
       window.location = vue.done_url;
     }
   }, 650);
}



function same(card1, card2) {
  return card1.term_id == card2.term_id;
}

function reset(vue, empty_card) {
  vue.choice1 = empty_card;
  vue.choice2 = empty_card;
}
