export var Quiz = {
  run: function(vueAppName, terms_url) {
    $.get( terms_url )
    .done(function( data ) {

      var items = data["data"];
      var index = 0;
      var incorrect_indexes = [];
      for (var i = 0; i <= items.length - 1; i++) {
        incorrect_indexes.push(i);
      }

      new Vue({
        el: vueAppName,
        data: {
          item: items[index],
          picked: '',
          size: items.length,
          wrong_answers: "",
          incorrect_indexes: incorrect_indexes,
          answered: false,
          term_ids: data["term_ids"] || ""
        },
        computed: {
          completed: function() {
            return this.size - this.incorrect_indexes.length;
          },
        },
        methods: {
          show_right_answer: function(choice) {
            if (this.answered && choice == this.item.answer) {
                return true;
            }
            return false;
          },
          show_wrong_answer: function(choice) {
            if (this.answered && this.picked == choice && this.picked != this.item.answer) {
              return true;
            }
            return false;
          },
          status: function(choice) {
            if (this.answered && choice == this.item.answer) {
                return "correct_answer";
            }
          },
          select: function(item, choice) {
            if (!this.answered) {
              if (item.answer == choice) {
                this.incorrect_indexes.splice(index, 1);
              } else {
                var term = items[this.incorrect_indexes[index]];
                this.wrong_answers += " " + term.term_id;
                index = index + 1;
              }
              this.answered = true;
            }
          },
          next: function (event) {
            this.answered = false;
            this.picked = '';
            if (this.incorrect_indexes.length < 1) {
              $('#done_quiz_form').submit();
            } else {
              if (index >= this.incorrect_indexes.length) {
                index = 0;
              }
              this.item = items[this.incorrect_indexes[index]];
            }
          },
        }
      }); //end of Vue
      $('.loading').hide();
      $('.content').show();
    }); //end of get
  }
}
