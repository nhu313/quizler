export var Deck = {
  run: function(vueAppName, terms_url, done_url) {
    $.get( terms_url )
    .done(function( data ) {

      var terms = data["data"];
      var index = 0;
      var size = terms.length;
      var last_index = size -1;

      new Vue({
        el: vueAppName,
        data: {
          term: terms[index],
          completed: 0,
          size: size
        },
        methods: {
          next: function (event) {
            if (index < last_index) {
              index = index + 1;
              updateView(this, terms[index], index);
            } else {
              window.location.href = done_url;
            }
          },
          back: function(event) {
            if (index > 0) {
              index = index - 1;
            }
            updateView(this, terms[index], index);
          },
          show_translation: function(event) {
            return this.term.translation.id != null;
          }
        }
      }); //end of Vue
      $('.loading').hide();
      $('.content').show();
    }); //end of get
  }
}


function updateView(vue, term, index) {
  vue.term = term;
  vue.completed = index;
}
