defmodule Quizler.ModelStringHelper do
  import Ecto.Changeset

  def clean(changeset, field) do
    value = clean(get_field(changeset, field))

    put_change(changeset, field, value)
  end

  defp clean(nil), do: nil
  defp clean(value), do: String.trim(value)
end
