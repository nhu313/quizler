defmodule Quizler.SpanishTerm do
  use Quizler.Web, :model

  schema "spanish_terms" do
    field :source_term_id, :integer
    field :term, :string
    field :definition, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:source_term_id, :term, :definition])
    |> validate_required([:source_term_id, :term])
  end

  def new_changeset(params) do
    %Quizler.SpanishTerm{}
    |> cast(params, [:source_term_id, :term, :definition])
    |> validate_required([:source_term_id, :term])
  end
end
