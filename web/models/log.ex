defmodule Quizler.Log do
  use Quizler.Web, :model

  schema "logs" do
    field :path, :string

    belongs_to :user, Quizler.User
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :path])
    |> validate_required([:path])
  end

  def new_changeset(params) do
    %Quizler.Log{}
    |> cast(params, [:user_id, :path])
    |> validate_required([:path])
  end
end
