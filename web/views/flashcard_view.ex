defmodule Quizler.FlashcardView do
  use Quizler.Web, :view

  def render("terms.json", %{terms: terms}) do
    %{data: render_many(terms, Quizler.FlashcardView, "term.json")}
  end

  def render("term.json", %{flashcard: term}) do
    %{term: term.term,
      definition: term.definition,
      image_url: term.image,
      id: term.id
      }
  end
end
