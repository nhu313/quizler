defmodule Quizler.ExamView do
  use Quizler.Web, :view

  def render("questions.json", %{exam: exam}) do
    %{
      name: exam.name,
      grouped_questions: exam.grouped_questions
    }
  end

  def score(exam) do
    result = hd(exam.results)
    num_of_questions = length(exam.questions)
    num_of_wrong = length(result.wrong_answers)
    "#{num_of_questions - num_of_wrong}/#{num_of_questions}"
  end

  def result(question) do
    if (question.answer == question.user_answer) do
      "text-success"
    else
      "text-danger"
    end
  end

end
