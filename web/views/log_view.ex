defmodule Quizler.LogView do
  use Quizler.Web, :view

  def log_user_name(nil), do: ""
  def log_user_name(user), do: user.name

end
