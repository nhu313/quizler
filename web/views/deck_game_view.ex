defmodule Quizler.Deck.GameView do
  use Quizler.Web, :view

  def render("cards_for_matching.json", %{cards: cards}) do
    cards
  end
end
