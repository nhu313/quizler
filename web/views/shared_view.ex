defmodule Quizler.SharedView do
  def logged_in?(conn) do
    Plug.Conn.get_session(conn, :user)
  end

  def user_photo(conn) do
    url = Plug.Conn.get_session(conn, :user).photo_url
    if url do
      url
    else
      "/images/user.png"
    end
  end

  def user_name(conn) do
    user = Plug.Conn.get_session(conn, :user)
    if user do
      user.name
    else
      ""
    end
  end

  def get_assigns(assigns, key) do
    case Access.fetch(assigns, key) do
        {:ok, value} -> value
        _ -> nil
    end
  end

  def admin?(conn) do
    Quizler.Admin.admin?(conn)
  end

  @one_pixel_img "/images/one_pixel.svg"
  def image_url(nil) do
    @one_pixel_img
  end

  def image_url(url) do
    url
  end
end
