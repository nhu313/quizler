defmodule Quizler.DeckView do
  use Quizler.Web, :view

  def render("terms.json", %{terms: terms}) do
    %{data: render_many(terms, Quizler.DeckView, "term.json")}
  end

  def render("term.json", %{deck: term}) do
    %{term: term.term,
      definition: term.definition,
      image_url: term.image,
      sentence: sentence(term),
      id: term.id,
      translation: %{
          id: term.translation.id,
          term: term.translation.term,
          definition: term.translation.definition
        }
      }
  end

  defp sentence(term) do
    sentences = term.sentences
    if length(sentences) > 0 do
      sentences
      |> Enum.shuffle
      |> hd
      |> Map.get(:sentence)
    else
      ""
    end
  end

  def render("items.json", %{items: items}) do
    IO.inspect "inspect in items not terms/ wtf"
    %{data: items}
  end
end
