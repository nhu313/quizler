defimpl Phoenix.HTML.Safe, for: Quizler.Language do
  def to_iodata(nil),  do: ""
  def to_iodata(language), do: language.name
end
