defmodule Quizler.GroupView do
  use Quizler.Web, :view

  def render("items.json", %{items: items}) do
    term_ids = items |> Enum.map(&(&1.term_id)) |> Enum.join(" ")
    %{data: render_many(items, Quizler.GroupView, "item.json"),
      term_ids: term_ids
    }
  end

  def render("item.json", %{group: item}) do
    item
  end
end
