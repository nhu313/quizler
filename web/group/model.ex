defmodule Quizler.Group do
  use Quizler.Web, :model

  schema "groups" do
    field :name, :string
    field :user_id, :integer
    field :step, :integer, default: 0

    has_many :decks, Quizler.Deck, on_delete: :delete_all
    has_many :terms, through: [:decks, :terms]

    has_one :current_deck, Quizler.Deck
    has_many :not_done_decks, Quizler.Deck

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :step])
    |> validate_required([:name])
  end

  def new_changeset(params) do
    %Quizler.Group{}
    |> cast(params, [:name, :step])
    |> validate_required([:name])
  end
end
