defmodule Quizler.GroupQuery do
  import Ecto.Query
  alias Quizler.{Repo, Group, Deck, QuizResult}

  def id(id) do
    Repo.get!(Group, id)
  end

  def all_with_decks(user_id) do
    query = from  group in Group, order_by: [asc: group.step]

    query
    |> Repo.all
    |> Repo.preload(decks: deck_query)
    |> Enum.map(&(split_deck_by_status(&1, user_id)))
  end

  def with_deck_split_by_status(id, user_id) do
    deck = deck_query
    query = from group in Group,
            where: group.id == ^id,
            preload: [decks: ^deck]

    Repo.one(query) |> split_deck_by_status(user_id)
  end

  defp deck_query do
    from deck in Deck, order_by: [asc: deck.step]
  end

  def with_terms(id) do
    query = from group in Group,
            where: group.id == ^id,
            preload: [:terms]

     Repo.one(query)
  end

  def split_deck_by_status(nil), do: nil

  def split_deck_by_status(group, user_id) do
    deck_ids = Enum.map(group.decks, &(&1.id))
    done_deck_ids = query_done_decks(deck_ids, user_id)

    {done_decks, not_done_decks} = Enum.split_while(group.decks, &(Enum.member?(done_deck_ids, &1.id)))

    group
    |> Map.put(:done_decks, done_decks)
    |> set_not_done_decks(not_done_decks)
  end

  defp set_not_done_decks(group, []) do
    group
    |> Map.put(:current_deck, nil)
    |> Map.put(:not_done_decks, [])
  end

  defp set_not_done_decks(group, [current | not_done]) do
    group
    |> Map.put(:current_deck, current)
    |> Map.put(:not_done_decks, not_done)
  end


  defp query_done_decks(deck_ids, user_id) do
    query = from d in QuizResult,
            where: d.user_id == ^user_id and d.deck_id in ^deck_ids,
            select: d.deck_id

    Repo.all(query)
  end

  def next(current_group) do
    query = from group in Group,
            where: group.step > ^current_group.step,
            limit: 1,
            order_by: [asc: :step]

    Repo.one(query)
  end
end
