defmodule Quizler.GroupController do
  use Quizler.Web, :controller

  alias Quizler.{Group, GroupQuery, TermQuery, Deck.QuizGenerator, UserTestGroup.Helper}
  plug Quizler.Authorizer
  plug Quizler.Admin when action in [:new, :create, :edit, :update, :delete, :test_results]

  def test(conn, %{"id" => id}) do
    group = Repo.get!(Group, id)
    render(conn, "test.html", group: group)
  end

  def test_items(conn, params) do
    test_items_version(conn, Helper.add_test_group(user(conn).id, params))
  end

  def test_items_version(conn, %{"id" => id, "test_group" => 2}) do
    items = id
            |> TermQuery.for_group_with_language(Quizler.SpanishTerm)
            |> QuizGenerator.create
            |> Enum.take(Application.get_env(:quizler, :number_of_test_questions))
    render(conn, "items.json", items: items)
  end

  def test_items_version(conn, %{"id" => id}) do
    items = id
            |> TermQuery.for_group(user(conn).id, Application.get_env(:quizler, :number_of_test_questions))
            |> QuizGenerator.create
    render(conn, "items.json", items: items)
  end

  def terms(conn, %{"id" => id}) do
    group = GroupQuery.with_terms(id)
    render(conn, "terms.html", group: group)
  end

  def index(conn, _params) do
    groups = GroupQuery.all_with_decks(user(conn).id)
    render(conn, "index.html", groups: groups)
  end

  def all(conn, _params) do
    groups = Repo.all(Group)
    render(conn, "all.html", groups: groups)
  end

  def test_done(conn, params) do
    Quizler.ResultSaver.create_test(params, user(conn).id)

    group = GroupQuery.id(params["group_id"])
    next_group = GroupQuery.next(group)
    render(conn, "test_done.html", group: group, next_group: next_group)
  end

  def new(conn, _params) do
    changeset = Group.changeset(%Group{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"group" => group_params}) do
    changeset = Group.changeset(%Group{}, group_params)

    case Repo.insert(changeset) do
      {:ok, _group} ->
        conn
        |> put_flash(:info, "Group created successfully.")
        |> redirect(to: group_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    group = Quizler.GroupQuery.with_deck_split_by_status(id, user(conn).id)
    render(conn, "show.html", group: group)
  end

  def edit(conn, %{"id" => id}) do
    group = Repo.get!(Group, id)
    changeset = Group.changeset(group)
    render(conn, "edit.html", group: group, changeset: changeset)
  end

  def update(conn, %{"id" => id, "group" => group_params}) do
    group = Repo.get!(Group, id)
    changeset = Group.changeset(group, group_params)

    case Repo.update(changeset) do
      {:ok, group} ->
        conn
        |> put_flash(:info, "Group updated successfully.")
        |> redirect(to: group_path(conn, :show, group))
      {:error, changeset} ->
        render(conn, "edit.html", group: group, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    group = Repo.get!(Group, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(group)

    conn
    |> put_flash(:info, "Group deleted successfully.")
    |> redirect(to: group_path(conn, :index))
  end

  def test_results(conn, _) do
    results = Repo.all(Quizler.TestResult)
    render(conn, "test_results.html", results: results)
  end

  def delete_test_result(conn, %{"test_result_id" => id}) do
    result = Repo.get!(Quizler.TestResult, id)

    Repo.delete(result)

    conn
    |> put_flash(:info, "Test result deleted successfully.")
    |> redirect(to: group_path(conn, :test_results))
  end
end
