defmodule Quizler.LanguageSaver do
  alias Quizler.{Repo, Language, UserLanguage}

  def save_user_languages(user, nil), do: []

  def save_user_languages(user, submitted_language_ids) do
    user = Repo.preload user, :languages
    language_ids = user.languages
              |> Enum.map(&(&1.id))
              |> Enum.sort

    sorted_submitted_language_ids = get_sorted_language_ids(submitted_language_ids)
    new_language_ids = sorted_submitted_language_ids -- language_ids
    Enum.map(new_language_ids, &(create_user_language(user, &1)))

    (language_ids -- sorted_submitted_language_ids)
      |> Enum.map(&(find_language(user.languages, &1)))
      |> Enum.map(&Repo.delete/1)

    sorted_submitted_language_ids
  end

  defp find_language(user_languages, language_id) do
    Enum.find(user_languages, &(&1.id == language_id))
  end

  defp create_user_language(user, language_id) do
    params = %{user_id: user.id, language_id: language_id}
    UserLanguage.changeset(%UserLanguage{}, params)
    |> Repo.insert!
  end

  defp get_sorted_language_ids(languages) do
    {new_languages, language_ids} = languages
                          |> Enum.map(&({&1, Integer.parse(&1)}))
                          |> Enum.partition(fn {_, value} -> value == :error end)

    new_language_ids = new_languages
                  |> Enum.map(&create_language/1)
                  |> Enum.filter_map(&(&1 != nil), &(&1.id))

    language_ids
    |> Enum.map(fn {_, {value, _}} -> value end)
    |> List.flatten(new_language_ids)
    |> Enum.sort
  end

  defp create_language({name, _}) do
    params = %{name: name}
    language = Repo.get_by(Language, params)

    if language do
      language
    else
      changeset = Language.changeset(%Language{}, params)

      case Repo.insert(changeset) do
        {:ok, language} -> language
        {:error, _} -> nil
      end
    end
  end
end
