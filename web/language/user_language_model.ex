defmodule Quizler.UserLanguage do
  use Quizler.Web, :model

  schema "users_languages" do
    field :user_id, :integer
    field :language_id, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :language_id])
    |> validate_required([:user_id, :language_id])
  end
end
