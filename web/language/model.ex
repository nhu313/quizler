defmodule Quizler.Language do
  use Quizler.Web, :model

  schema "languages" do
    field :name, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
  end

  def new_changeset(params) do
    %Quizler.Language{}
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
