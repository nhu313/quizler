defmodule Quizler.SeederTerm do
  alias Quizler.{Repo}

  def run(file_name, group_name) do
    file_name
    |> File.read!
    |> Poison.decode!
    |> Enum.shuffle
    |> Enum.chunk(45)
    |> Enum.with_index
    |> Enum.map(&(create_group(group_name, &1)))
  end

  defp create_group(group_name, {terms, index}) do
    Quizler.SeederGroup.create_group(terms, group_name, index + 1, 15)
  end
end

defmodule Quizler.SeederTermSmall do
  alias Quizler.{Repo, SeederGroup}

  def run(file_name, group_name) do
    file_name
    |> File.read!
    |> Poison.decode!
    |> Enum.shuffle
    |> SeederGroup.create_group(group_name, 1, 3)
  end
end
