defmodule Quizler.SeederGroup do
  alias Quizler.{Repo, Term, Group, Deck, TermSentence}

  def create_group(terms, group_name, step, size) do
    group = create_one_group(group_name, step)

    terms
    |> Enum.chunk(size)
    |> Enum.with_index
    |> Enum.map(&create_deck_and_terms(&1, group))
  end

  defp create_one_group(name, step) do
    %{name: "#{name} #{step}", step: (step * 2)}
    |> Group.new_changeset
    |> Repo.insert!
  end

  def create_deck_and_terms({terms, index}, group) do
    deck = create_deck(group, index)
    Enum.map(terms, &create_term(&1, deck))
  end

  defp create_deck(group, index) do
    %{name: "Day #{index + 1}", group_id: group.id,
      image: get_image(index), step: index + 1}
    |> Deck.new_changeset
    |> Repo.insert!
  end

  @img_path "/images/group/"
  defp get_image(index) do
    case rem(index, 4) do
      0 -> @img_path <> "book.png"
      1 -> @img_path <> "scale.png"
      2 -> @img_path <> "gravel.png"
      _ -> @img_path <> "paper.png"
    end
  end

  defp create_term(params, deck) do
    params
    |> Map.put("deck_id", deck.id)
    |> Term.new_changeset
    |> Repo.insert!
    |> add_sentences(params)
  end

  defp add_sentences(term, params) do
    sentences = Map.get(params, "sentences")
    if sentences do
      Enum.map(sentences, &(create_sentence(&1, term)))
    end
  end

  defp create_sentence(sentence, term) do
    %{term_id: term.id, sentence: sentence}
    |> TermSentence.new_changeset
    |> Repo.insert!
  end
end
