defmodule Quizler.SeederExam do
  alias Quizler.{Repo, Exam, ExamQuestion, ExamQuestionType}

  def run(file_name, exam_name, code \\ "") do
    exam = create_exam(exam_name, code)
    types = get_question_types

    file_name
    |> File.read!
    |> Poison.decode!
    |> Enum.map(&(create_question(&1, exam, types)))
  end

  defp create_exam(exam_name, code) do
    %{name: exam_name, code: code}
    |> Exam.new_changeset
    |> Repo.insert!
  end

  defp create_question(question, exam, types) do
    [type] = types[question["type"]]
    question
    |> Map.put("exam_id", exam.id)
    |> Map.put("type_id", type.id)
    |> ExamQuestion.new_changeset
    |> Repo.insert!
  end

  defp get_question_types do
    Repo.all(ExamQuestionType)
    |> Enum.group_by(&(&1.name))
  end
end
