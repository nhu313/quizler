defmodule Quizler.SeederLanguage do
  alias Quizler.{Repo, Language}

  def run do
    languages
    |> Enum.map(&create/1)
  end

  defp create(language) do
    %{name: language}
    |> Language.new_changeset
    |> Repo.insert!
  end

  defp languages do
    ["Abkhaz", "Adangme", "Adyghe", "Afrikaans", "Albanian", "Altay", "Amharic", "Arabic", "Armenian",
     "Assamese", "Aymara", "Azerbaijani", "Balinese", "Balochi", "Bashkir", "Basque", "Belarusian",
     "Bengali", "Bikol", "Bislama", "Bodo", "Bosnian", "Bulgarian", "Buriat", "Burmese", "Cantonese",
     "Carolinian", "Catalan", "Cebuano", "Chamorro", "Chechen", "Chichewa", "Chipewyan", "Chukchi",
     "Chuvash", "Comorian", "Cornish", "Cree", "Crioulo", "Croatian", "Czech", "Dagaare", "Dagbani",
     "Danish", "Dari", "Dgèrnésiais", "Dhivehi", "Dogri", "Dogrib", "Dolgan", "Dutch", "Dzongkha",
     "Erzya", "Estonian", "Evenk", "Ewe", "Faroese", "Fijian", "Filipino", "Finnish",
     "Franco-Provençal", "French", "Frisian", "Friulian", "Fula", "Ga", "Gagauz", "Galician",
     "Georgian", "German", "Gonja", "Greek", "Guaraní", "Gujarati", "Gwich'in", "Haitian Creole",
     "Hausa", "Hawaiian", "Hebrew", "Hiligaynon", "Hindi", "Hindustani", "Hiri Motu", "Hungarian",
     "Icelandic", "Igbo", "Ilokano", "Indonesian", "Ingush", "Inuinnaqtun", "Inuktitut", "Irish",
     "Italian", "Japanese", "Javanese", "Jèrriais", "Jula", "Kabardian", "Kalaallisut", "Kalanga",
     "Kalmyk", "Kannada", "Kapampangan", "Karachay-Balkar", "Kasem", "Kashmiri", "Kazakh", "Khakas",
     "Khanty", "Khmer", "Kikongo", "Kinaray-a", "Kinyarwanda", "Kirghiz", "Kiribati", "Kirundi",
     "Komi-Permyak", "Komi-Zyrian", "Konkani", "Korean", "Koryak", "Kurdish", "Ladin", "Lao", "Latvian",
     "Lingala", "Lithuanian", "Lower Sorbian", "Luxembourgish", "Maguindanao", "Maithili", "Malagasy",
     "Malay", "Malayalam", "Maltese", "Mandarin", "Manipuri", "Mansi", "Maori", "Maranao", "Marathi",
     "Mari", "Marshallese", "Meänkieli", "Moksha", "Moldovan", "Mongolian", "Moore", "Munukutuba",
     "Nauruan", "Ndebele", "Nenets", "Nepali", "New Zealand Sign Language", "Niuean", "Northern Sotho",
     "Norwegian", "Nuristani", "Nzema", "Occitan", "Oriya", "Ossetic", "Palauan", "Pamiri", "Pangasinan",
     "Pashai", "Pashto", "Persian", "Pitcairnese", "Polish", "Portuguese", "Punjabi", "Quechua", "Romani",
     "Romanian", "Romansh", "Russian", "Ruthenian", "Sami", "Samoan", "Sango", "Sanskrit", "Santali",
     "Sardinian", "Scots", "Scottish Gaelic", "Serbian", "Seselwa", "Sign Language", "Sindhi", "Sinhala",
     "Slavey", "Slovak", "Slovenian", "Somali", "Soninke", "Sotho", "Spanish", "Sundanese", "Swahili",
     "Swazi", "Swedish", "Tagalog", "Tahitian", "Tajik", "Tamazight", "Tamil", "Tatar", "Tausug", "Telugu",
     "Thai", "Tibetan", "Tigrinya", "Tok Pisin", "Tokelauan", "Tongan", "Tshiluba", "Tsonga", "Tswana",
     "Turkish", "Turkmen", "Tuvaluan", "Tuvin", "Twi", "Udmurt", "Ukrainian", "Upper Sorbian", "Urdu",
     "Uyghur", "Uzbek", "Venda", "Vietnamese", "Waray-Waray", "Welsh", "Wolof", "Xhosa", "Yakut", "Yiddish",
     "Yoruba", "Zhuang", "Zulu"]
  end
end
