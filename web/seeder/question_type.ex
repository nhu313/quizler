defmodule Quizler.SeederExamQuestionType do
  alias Quizler.{Repo, ExamQuestionType}

  def run do
    types = [
      %{name: "Ethics"},
      %{name: "Legal Terms"},
      %{name: "Legal Terms - Sentence Completion", instruction: "Select the legal phrase or definition that best completes the given statement."},
      %{name: "Slangs"},
      %{name: "Vocabulary"},
      %{name: "Vocabulary - Sentence Completion", instruction: "Choose the word that best completes the given sentence."},
      %{name: "Synonyms", instruction: "Select the word that most closely expresses the same meaning as the target word."},
      %{name: "Antonyms", instruction: "Select the word that most closely has the opposite meaning of the target word."},
      %{name: "Idioms", instruction: "Select the word/phrase that best conveys the meaning of the idiomatic expression."},
      %{name: "Grammar"}
    ]

    Enum.map(types, &create/1)
  end

  defp create(type) do
    type
    |> ExamQuestionType.new_changeset
    |> Repo.insert!
  end
end
