defmodule Quizler.UserTestGroupController do
  use Quizler.Web, :controller

  alias Quizler.UserTestGroup
  plug Quizler.Admin

  def index(conn, _params) do
    users_test_groups = Repo.all(UserTestGroup)
    render(conn, "index.html", users_test_groups: users_test_groups)
  end

  def new(conn, _params) do
    changeset = UserTestGroup.changeset(%UserTestGroup{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user_test_group" => user_test_group_params}) do
    changeset = UserTestGroup.changeset(%UserTestGroup{}, user_test_group_params)

    case Repo.insert(changeset) do
      {:ok, _user_test_group} ->
        conn
        |> put_flash(:info, "User test group created successfully.")
        |> redirect(to: user_test_group_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user_test_group = Repo.get!(UserTestGroup, id)
    render(conn, "show.html", user_test_group: user_test_group)
  end

  def edit(conn, %{"id" => id}) do
    user_test_group = Repo.get!(UserTestGroup, id)
    changeset = UserTestGroup.changeset(user_test_group)
    render(conn, "edit.html", user_test_group: user_test_group, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user_test_group" => user_test_group_params}) do
    user_test_group = Repo.get!(UserTestGroup, id)
    changeset = UserTestGroup.changeset(user_test_group, user_test_group_params)

    case Repo.update(changeset) do
      {:ok, user_test_group} ->
        conn
        |> put_flash(:info, "User test group updated successfully.")
        |> redirect(to: user_test_group_path(conn, :show, user_test_group))
      {:error, changeset} ->
        render(conn, "edit.html", user_test_group: user_test_group, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_test_group = Repo.get!(UserTestGroup, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(user_test_group)

    conn
    |> put_flash(:info, "User test group deleted successfully.")
    |> redirect(to: user_test_group_path(conn, :index))
  end
end
