defmodule Quizler.UserTestGroup.Helper do
  alias Quizler.{UserTestGroup, Repo}

  def add_test_group(user_id, params) do
    test = test_group(user_id)
    Map.put(params, "test_group", test.group_id)
  end

  defp test_group(user_id) do
    Repo.get_by(UserTestGroup, user_id: user_id) || %{group_id: 1}
  end
end
