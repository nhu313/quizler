defmodule Quizler.UserTestGroup do
  use Quizler.Web, :model

  schema "users_test_groups" do
    field :user_id, :integer
    field :group_id, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :group_id])
    |> validate_required([:user_id, :group_id])
  end
end
