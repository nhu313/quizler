defmodule Quizler.Logger do
  import Plug.Conn

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    user = get_session(conn, :user) || %{id: 0}

    %{user_id: user.id, path: conn.request_path}
    |> Quizler.Log.new_changeset
    |> repo.insert

    conn
  end
end
