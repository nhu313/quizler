defmodule Quizler.TranslationTerm do
  use Ecto.Schema

  schema "abstract table for language" do
    field :source_term_id, :integer
    field :term, :string
    field :definition, :string
  end

end
