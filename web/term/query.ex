defmodule Quizler.TermQuery do
  import Ecto.Query

  alias Quizler.{Repo, Term, Group, Deck, QuizResult, TestResult,
                 TranslationTerm, DeckQuery}
  @one_pixel_img "/images/one_pixel.svg"

  def for_deck(deck_id) do
    query = from term in Term,
            where: term.deck_id == ^deck_id

    query |> Repo.all |> Enum.map(&add_image/1)
  end

  def for_deck_with_language(deck_id, translation_model) do
      translation_query = from lang in translation_model

    query = from term in Term,
            where: term.deck_id == ^deck_id,
            preload: [:sentences, translation: ^translation_query]

    query
    |> Repo.all
    |> Enum.map(&add_image/1)
    |> Enum.map(&add_translation/1)
  end

  def for_deck_replace_term_with_language(deck_id, model) do
    query = from deck in Deck,
            where: deck.id == ^deck_id,
            join: term in Term, on: term.deck_id == deck.id,
            join: trans in ^model, on: trans.source_term_id == term.id,
            select: %{id: term.id,
                      term: trans.term,
                      definition: term.definition,
                      image: term.image}

    query |> Repo.all |> Enum.map(&add_image/1)
  end

  def for_group(group_id, user_id, size \\ 25) do
    decks = DeckQuery.for_group(group_id) |> Repo.preload(:terms)
    terms = decks |> Enum.map(&(&1.terms)) |> List.flatten

    if length(terms) <= size do
      terms
    else
      deck_ids = decks |> Enum.map(&(&1.id))
      quiz_results = get_quiz_results(deck_ids, user_id)
      test_results = get_test_results(group_id, user_id)

      tested_term_ids = test_results |> Enum.map(&(&1.term_ids)) |> List.flatten

      {quizzed_terms, non_quizzed_terms} = separate_quizzed_terms(decks, quiz_results)
      quizzed_term_ids = quizzed_terms |> Enum.map(&(&1.id))
      non_quizzed_term_ids = non_quizzed_terms |> Enum.map(&(&1.id))

      never_seen_term_ids = non_quizzed_term_ids -- tested_term_ids
      {never_seen_terms, tested_terms} = Enum.partition(non_quizzed_terms, &(Enum.member?(never_seen_term_ids, &1.id)))

      terms = tested_terms ++ quizzed_terms
      seen_terms = sort_seen_terms(terms, test_results, quiz_results, tested_term_ids, quizzed_term_ids)

      never_seen_terms ++ seen_terms
      |> Enum.uniq
      |> Enum.take(size)
    end
  end

  defp separate_quizzed_terms(decks, quiz_results) do
    deck_id_with_quiz_results = quiz_results |> Enum.map(&(&1.deck_id)) |> Enum.uniq
    {deck_with_results, deck_without_results} = Enum.partition(decks, &(Enum.member?(deck_id_with_quiz_results, &1.id)))

    quizzed_terms = deck_with_results |> Enum.map(&(&1.terms)) |> List.flatten
    non_quizzed_terms = deck_without_results |> Enum.map(&(&1.terms)) |> List.flatten
    {quizzed_terms, non_quizzed_terms}
  end

  defp sort_seen_terms(terms, test_results, quiz_results, tested_term_ids, quizzed_term_ids) do
    wrong_ids = get_wrong_answer_ids(test_results) ++ get_wrong_answer_ids(quiz_results)
                |> Enum.reduce(%{}, fn(x, acc) -> Map.put(acc, x, Map.get(acc, x, 0) + 1) end)

    {seen_once_ids, seen_ids} = tested_term_ids ++ quizzed_term_ids
                                    |> Enum.reduce(%{}, fn(x, acc) -> Map.put(acc, x, Map.get(acc, x, 0) + 1) end)
                                    |> Enum.partition(fn {_, count} -> count === 1 end)

    seen_ids = seen_ids
              |> Enum.into(%{})
              |> Map.merge(wrong_ids, fn(_k, v1, v2) -> (v1 - v2)/v1 end)
              |> Enum.into([])
              |> Enum.sort(fn({_, count1}, {_, count2}) -> count1 < count2 end)

    {wrong_once_ids, right_once_ids} = Enum.partition(seen_once_ids, fn {id, _} -> Map.get(wrong_ids, id) == 1 end)

    wrong_once_ids ++ right_once_ids ++ seen_ids
    |> Enum.map(fn {id, count} -> Enum.find(terms, fn x -> x.id == id end) end)
  end

  defp get_wrong_answer_ids(results) do
    results
    |> Enum.map(fn x -> x.wrong_answers end)
    |> List.flatten
  end

  defp get_test_results(group_id, user_id) do
    query = from result in TestResult,
            where: result.user_id == ^user_id and result.group_id == ^group_id,
            select: result

    Repo.all(query)
  end

  defp get_quiz_results(deck_ids, user_id) do
    query = from result in QuizResult,
            where: result.user_id == ^user_id and result.deck_id in ^deck_ids,
            select: result

    Repo.all(query)
  end

  def for_group_with_language(group_id, model) do
    query = from group in Group,
            where: group.id == ^group_id,
            join: deck in Deck, on: deck.group_id == group.id,
            join: term in Term, on: term.deck_id == deck.id,
            join: trans in ^model, on: trans.source_term_id == term.id,
            select: %{id: term.id,
                      term: trans.term,
                      definition: term.definition,
                      image: term.image}

    query |> Repo.all |> Enum.map(&add_image/1)
  end

  defp add_image(term) do
    if term.image do
      term
    else
      Map.put(term, :image, @one_pixel_img)
    end
  end

  defp add_translation(term) do
    if term.translation do
      term
    else
      Map.put(term, :translation, %TranslationTerm{})
    end
  end
end
