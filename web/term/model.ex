defmodule Quizler.Term do
  @derive [Poison.Encoder]
  use Quizler.Web, :model
  alias Quizler.ModelStringHelper

  schema "terms" do
    field :term, :string
    field :definition, :string
    field :image, :string

    belongs_to :deck, Quizler.Deck
    has_one :translation, Quizler.TranslationTerm, foreign_key: :source_term_id
    has_many :sentences, Quizler.TermSentence

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:term, :definition, :image, :deck_id])
    |> validate_required([:term, :definition])
    |> ModelStringHelper.clean(:definition)
    |> ModelStringHelper.clean(:term)
  end

  def new_changeset(params) do
    %Quizler.Term{}
    |> cast(params, [:term, :definition, :image, :deck_id])
    |> validate_required([:term])
  end
end
