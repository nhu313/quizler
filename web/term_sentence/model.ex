defmodule Quizler.TermSentence do
  use Quizler.Web, :model

  @derive {Poison.Encoder, only: [:sentence]}
  schema "term_sentences" do
    field :sentence, :string

    belongs_to :term, Quizler.Term

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:term_id, :sentence])
    |> validate_required([:term_id, :sentence])
  end

  def new_changeset(params) do
    changeset(%Quizler.TermSentence{}, params)
  end
end
