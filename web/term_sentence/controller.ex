defmodule Quizler.TermSentenceController do
  use Quizler.Web, :controller

  alias Quizler.TermSentence
  plug Quizler.Admin

  def index(conn, _params) do
    term_sentences = Repo.all(TermSentence)
    render(conn, "index.html", term_sentences: term_sentences)
  end

  def new(conn, _params) do
    changeset = TermSentence.changeset(%TermSentence{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"term_sentence" => term_sentence_params}) do
    changeset = TermSentence.changeset(%TermSentence{}, term_sentence_params)

    case Repo.insert(changeset) do
      {:ok, _term_sentence} ->
        conn
        |> put_flash(:info, "Term sentence created successfully.")
        |> redirect(to: term_sentence_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    term_sentence = Repo.get!(TermSentence, id)
    render(conn, "show.html", term_sentence: term_sentence)
  end

  def edit(conn, %{"id" => id}) do
    term_sentence = Repo.get!(TermSentence, id)
    changeset = TermSentence.changeset(term_sentence)
    render(conn, "edit.html", term_sentence: term_sentence, changeset: changeset)
  end

  def update(conn, %{"id" => id, "term_sentence" => term_sentence_params}) do
    term_sentence = Repo.get!(TermSentence, id)
    changeset = TermSentence.changeset(term_sentence, term_sentence_params)

    case Repo.update(changeset) do
      {:ok, term_sentence} ->
        conn
        |> put_flash(:info, "Term sentence updated successfully.")
        |> redirect(to: term_sentence_path(conn, :show, term_sentence))
      {:error, changeset} ->
        render(conn, "edit.html", term_sentence: term_sentence, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    term_sentence = Repo.get!(TermSentence, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(term_sentence)

    conn
    |> put_flash(:info, "Term sentence deleted successfully.")
    |> redirect(to: term_sentence_path(conn, :index))
  end
end
