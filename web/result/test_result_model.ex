defmodule Quizler.TestResult do
  use Quizler.Web, :model

  schema "test_results" do
    field :user_id, :integer
    field :group_id, :integer
    field :wrong_answers, {:array, :integer}
    field :term_ids, {:array, :integer}
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :group_id, :wrong_answers, :term_ids])
    |> validate_required([:user_id, :group_id])
  end

  def new_changeset(params) do
    %Quizler.TestResult{}
    |> cast(params, [:user_id, :group_id, :wrong_answers, :term_ids])
    |> validate_required([:user_id, :group_id])
  end
end
