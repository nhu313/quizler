defmodule Quizler.ResultSaver do
  alias Quizler.{Repo, QuizResult, TestResult, ExamQuestionQuery, ExamResult}

  def create_for_exam(params, user_id) do
    questions = ExamQuestionQuery.exam(params["exam_id"])
    wrong_answers = get_wrong_answers(questions, params["answers"])
    create(params, user_id, ExamResult, wrong_answers)
  end

  defp get_wrong_answers(questions, answers) do
    func = fn(x, acc) -> get_wrong_answer(x, acc, answers) end
    Enum.reduce(questions, [], func)
  end

  defp get_wrong_answer(question, wrong_answers, user_answers) do
    answer = Map.get(user_answers, Integer.to_string(question.id))
    if (answer == question.answer) do
      wrong_answers
    else
      [question.id | wrong_answers]
    end
  end

  def create_test(params, user_id) do
    term_ids = params["result"]["term_ids"]
                |> split
                |> Enum.map(&parse_int/1)

    params
    |> Map.put("term_ids", term_ids)
    |> parse_and_create(user_id, TestResult)
  end

  def create_quiz(params, user_id) do
    parse_and_create(params, user_id, QuizResult)
  end

  defp parse_and_create(params, user_id, model) do
    ids = params["result"]["wrong_answers"]
          |> split
          |> Enum.map(&parse_int/1)
          |> Enum.uniq

    create(params, user_id, model, ids)
  end

  defp create(params, user_id, model, wrong_answers) do
    params
    |> Map.put("wrong_answers", wrong_answers)
    |> Map.put("user_id", user_id)
    |> model.new_changeset
    |> Repo.insert
  end

  defp parse_int(value) do
    {num, _} = Integer.parse(value);
    num
  end

  defp split(nil) do
    []
  end

  defp split(values) do
    String.split(values)
  end
end
