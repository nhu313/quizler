defmodule Quizler.QuizResult do
  use Quizler.Web, :model

  schema "quiz_results" do
    field :user_id, :integer
    field :deck_id, :integer
    field :wrong_answers, {:array, :integer}

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :deck_id, :wrong_answers])
    |> validate_required([:user_id, :deck_id])
  end

  def new_changeset(params) do
    %Quizler.QuizResult{}
    |> cast(params, [:user_id, :deck_id, :wrong_answers])
    |> validate_required([:user_id, :deck_id])
  end
end
